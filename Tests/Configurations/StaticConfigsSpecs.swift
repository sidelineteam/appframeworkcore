//
//  StaticConfigsSpecs.swift
//  AppFrameworkCore
//
//  Created by Jorge Miguel on 18/02/2025.
//

import XCTest
@testable import AppFrameworkCore

final class StaticConfigsTests: XCTestCase {
    
    // Test plist content
    private let testPlistContent = """
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>app</key>
    <dict>
        <key>name</key>
        <string>TestApp</string>
        <key>bundle_id</key>
        <string>com.test.app</string>
    </dict>
    <key>appframework</key>
    <dict>
        <key>analytics</key>
        <dict>
            <key>provider</key>
            <dict>
                <key>crashlytics</key>
                <true/>
                <key>firebase</key>
                <false/>
            </dict>
        </dict>
        <key>feedback</key>
        <dict>
            <key>email_address</key>
            <string>test@example.com</string>
            <key>key</key>
            <string>test_key_123</string>
        </dict>
    </dict>
</dict>
</plist>
"""
    
    private var tempPlistURL: URL!
    private var configuration: StaticConfiguration!
    
    override func setUp() {
        super.setUp()
        // Create temporary plist file
        let tempDir = FileManager.default.temporaryDirectory
        tempPlistURL = tempDir.appendingPathComponent("test_config.plist")
        try! testPlistContent.write(to: tempPlistURL, atomically: true, encoding: .utf8)
        
        // Initialize configuration
        configuration = StaticConfiguration(plistPath: tempPlistURL.path)
        XCTAssertNotNil(configuration, "Configuration should be initialized successfully")
    }
    
    override func tearDown() {
        // Clean up temporary file
        try? FileManager.default.removeItem(at: tempPlistURL)
        super.tearDown()
    }
    
    func testAppConfigurations() {
        // Test app name
        let nameConfig = StaticConfig<String>("app.name", defaultValue: "{APP_NAME}")
        XCTAssertEqual(configuration.get(nameConfig), "TestApp")
        
        // Test bundle ID
        let bundleConfig = StaticConfig<String>("app.bundle_id", defaultValue: "{APP_BUNDLE_ID}")
        XCTAssertEqual(configuration.get(bundleConfig), "com.test.app")
    }
    
    func testAnalyticsConfigurations() {
        // Test crashlytics enabled
        let crashlyticsConfig = StaticConfig<Bool>("appframework.analytics.provider.crashlytics", defaultValue: false)
        XCTAssertTrue(configuration.get(crashlyticsConfig))
        
        // Test firebase enabled
        let firebaseConfig = StaticConfig<Bool>("appframework.analytics.provider.firebase", defaultValue: true)
        XCTAssertFalse(configuration.get(firebaseConfig))
    }
    
    func testFeedbackConfigurations() {
        // Test email address
        let emailConfig = StaticConfig<String>("appframework.feedback.email_address", defaultValue: "")
        XCTAssertEqual(configuration.get(emailConfig), "test@example.com")
        
        // Test feedback key
        let keyConfig = StaticConfig<String>("appframework.feedback.key", defaultValue: "", encrypted: true)
        XCTAssertEqual(configuration.get(keyConfig), "test_key_123")
    }
    
    func testDefaultValues() {
        // Test non-existent key returns default value
        let nonExistentConfig = StaticConfig<String>("non.existent.key", defaultValue: "default")
        XCTAssertEqual(configuration.get(nonExistentConfig), "default")
    }
    
    func testDifferentValueTypes() {
        // Add test values to plist content
        let testContent = """
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>types</key>
    <dict>
        <key>int_value</key>
        <integer>42</integer>
        <key>float_value</key>
        <string>3.14</string>
        <key>double_value</key>
        <string>3.14159</string>
        <key>url_value</key>
        <string>https://example.com</string>
    </dict>
</dict>
</plist>
"""
        
        try! testContent.write(to: tempPlistURL, atomically: true, encoding: .utf8)
        let typeConfig = StaticConfiguration(plistPath: tempPlistURL.path)!
        
        // Test Integer
        let intConfig = StaticConfig<Int>("types.int_value", defaultValue: 0)
        XCTAssertEqual(typeConfig.get(intConfig), 42)
        
        // Test Float
        let floatConfig = StaticConfig<Float>("types.float_value", defaultValue: 0.0)
        XCTAssertEqual(typeConfig.get(floatConfig), 3.14)
        
        // Test Double
        let doubleConfig = StaticConfig<Double>("types.double_value", defaultValue: 0.0)
        XCTAssertEqual(typeConfig.get(doubleConfig), 3.14159)
        
        // Test URL
        let urlConfig = StaticConfig<URL>("types.url_value", defaultValue: URL(string: "https://default.com")!)
        XCTAssertEqual(typeConfig.get(urlConfig).absoluteString, "https://example.com")
    }
}
