//
//  UIImage.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 01/08/2018.
//  Copyright (c) 2018 Jorge Miguel. All rights reserved.
//

#if os(iOS)

import UIKit

extension UIImage {

    //-- MARk: constructors

    public static func load(_ named: String, resourcesBundle bundleName: String = "AppFrameworkV2") -> UIImage? {
        guard let frameworkBundle = Bundle.allBundles.first(where: { bundle -> Bool in
            return bundle.resourcePath?.contains(bundleName) ?? false
        }) else {

            let bundleURL = Bundle.main.resourceURL?.appendingPathComponent("\(bundleName).bundle")
            let resourceBundle = Bundle(url: bundleURL!)
            return UIImage(named: named, in: resourceBundle, compatibleWith: nil)
        }

        return UIImage(named: named, in: frameworkBundle, compatibleWith: nil)
    }

    //-- MARK: effects

    public var effectNoir: UIImage {
        let context = CIContext(options: nil)
        let currentFilter = CIFilter(name: "CIPhotoEffectNoir")!
        currentFilter.setValue(CIImage(image: self), forKey: kCIInputImageKey)
        let output = currentFilter.outputImage!
        let cgImage = context.createCGImage(output, from: output.extent)!
        let processedImage = UIImage(cgImage: cgImage, scale: scale, orientation: imageOrientation)

        return processedImage
    }

    //-- MARK: utils
    
    public var normalizedOrientation: UIImage? {
        guard self.imageOrientation != .up else {
            return self
        }

        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: self.size))
        let normalizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return normalizedImage
    }

    public func resize(_ size: CGSize, isOpaque: Bool = false) -> UIImage {

        //-- somehow way faster than calling the self.ratio var
        let horizontalRatio = size.width / self.size.width
        let verticalRatio = size.height / self.size.height
        let ratio = max(horizontalRatio, verticalRatio)

        let size = CGSize(width: self.size.width * ratio, height: self.size.height * ratio)

        let renderFormat = UIGraphicsImageRendererFormat.default()
        renderFormat.opaque = isOpaque

        let renderer = UIGraphicsImageRenderer(size: CGSize(width: size.width, height: size.height), format: renderFormat)
        return renderer.image { context in
            self.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        }
    }
    
    public static func resize(from path: URL, size: CGSize) -> UIImage {
        
        let options: [CFString: Any] = [
                kCGImageSourceCreateThumbnailFromImageIfAbsent: true,
                kCGImageSourceCreateThumbnailWithTransform: true,
                kCGImageSourceShouldCacheImmediately: true,
                kCGImageSourceThumbnailMaxPixelSize: max(size.width, size.height)
            ]

        guard let imageSource = CGImageSourceCreateWithURL(path as NSURL, nil),
             let image = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, options as CFDictionary) else {
        
            return UIImage()
        }

        return UIImage(cgImage: image)
    }

    public var ratio: CGFloat {
        let horizontalRatio = size.width / self.size.width
        let verticalRatio = size.height / self.size.height
        return max(horizontalRatio, verticalRatio)
    }
}

#endif
