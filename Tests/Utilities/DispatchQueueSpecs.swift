//
//  DispatchQueueSpecs.swift
//  AppFrameworkTests
//
//  Created by Jorge Miguel on 27/04/2020.
//  Copyright © 2020 JMIguel. All rights reserved.
//

import XCTest

@testable import AppFrameworkCore

class DispatchQueueSpecs: XCTestCase {
    
    //-- MARK: Dispatch on Main
        
    func testSyncOnMain() {
        var executedOnMain: Bool = true
        
        DispatchQueue.main.async {
            DispatchQueue.syncOnMain { executedOnMain = Thread.current.isMainThread }
            XCTAssertTrue(executedOnMain)
        }
        
        
        DispatchQueue.global().async {
            DispatchQueue.syncOnMain { executedOnMain = Thread.current.isMainThread }
            XCTAssertTrue(executedOnMain)
        }
    }
    
    func testAsyncOnMain() {
        let expectation = XCTestExpectation(description: "Execute async on main thread")
        var executedOnMain = false
        
        DispatchQueue.global().async {
            DispatchQueue.asyncOnMain {
                executedOnMain = Thread.current.isMainThread
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: 1.0)
        XCTAssertTrue(executedOnMain)
    }
    
    //-- MARK: Dispatch once
    
    func testOnce() {
        let token = "token_once"
        var count = 0
        
        let r1 = DispatchQueue.once(token: token) { count += 1 }
        let r2 = DispatchQueue.once(token: token) { count += 1 }
        
        XCTAssertEqual(count, 1)
        XCTAssertEqual(r1, true)
        XCTAssertEqual(r2, false)
    }
    
    func testResetOnce() {
        let token = "token_reset"
        var count = 0
        
        let r1 = DispatchQueue.once(token: token) { count = 1 }
        let r2 = DispatchQueue.once(token: token) { count = 2 }
        
        DispatchQueue.resetOnce(token: token)
        
        let r3 = DispatchQueue.once(token: token) { count = 3 }
        let r4 = DispatchQueue.once(token: token) { count = 4 }
        
        XCTAssertEqual(count, 3)
        XCTAssertEqual(r1, true)
        XCTAssertEqual(r2, false)
        XCTAssertEqual(r3, true)
        XCTAssertEqual(r4, false)
    }
}
