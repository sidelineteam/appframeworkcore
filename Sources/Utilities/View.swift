//
//  View.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 20/06/2021.
//  Copyright (c) 2021 Jorge Miguel. All rights reserved.
//

import SwiftUI

extension View {
    
    //-- MARK: generalize the view
    
    public func eraseToAnyView() -> AnyView { AnyView(self) }
    
    //-- MARK: modifier
    @available(*, deprecated, message: "⚠️ Warning: This method has caused unexpected behavior with navigation and view updates in the past. Consider using other options.")
    public func modify<T: View>(@ViewBuilder _ modifier: (Self) -> T) -> some View {
        modifier(self)
    }
    
    //-- MARK: conditional modifier
    
    @ViewBuilder
    public func `if`<Transform: View>(_ condition: Bool, transform: (Self) -> Transform) -> some View {
        if condition {
            transform(self)
        
        } else {
            self
        }
    }
}
