//
//  LocalNotifications.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 27/10/2020.
//  Copyright (c) 2020 Jorge Miguel. All rights reserved.
//

import Foundation
import UserNotifications

public struct LocalNotifications {
    
    public static func requestAuthorization() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in }
    }

    public static func schedule(title: String, message: String = "") {
        DispatchQueue.once {
            self.requestAuthorization()
        }
        
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = message
        content.sound = UNNotificationSound.default

        //-- Deliver the notification in a second.
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest.init(identifier: UUID().uuidString, content: content, trigger: trigger)

        //-- Schedule the notification.
        UNUserNotificationCenter.current().add(request)
    }

    public static func clearAllNotifications() {
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    }
}
