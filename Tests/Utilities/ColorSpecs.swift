//
//  ColorSpecs.swift
//  AppFrameworkTests
//
//  Created by Jorge Miguel on 29/04/2020.
//  Copyright © 2020 JMIguel. All rights reserved.
//

import XCTest
import SwiftUI

@testable import AppFrameworkCore

class ColorSpecs: XCTestCase {
    
    func testHex() {
        let red = Color(hex: "FF0000")
        let green = Color(hex: "00FF00")
        let blue = Color(hex: "#0000FF")
        let white = Color(hex: "FFFFFE")
        let black = Color(hex: "#000001")
        
        XCTAssertEqual(red.description, "#FF0000FF")
        XCTAssertEqual(green.description, "#00FF00FF")
        XCTAssertEqual(blue.description, "#0000FFFF")
        XCTAssertEqual(white.description, "#FFFFFEFF")
        XCTAssertEqual(black.description, "#000001FF")
    }
    
    func testUIColor() {
        let code = "#808080"
        let color = Color(hex: code)
        let uicolor = color.uiColor
        
        var red: CGFloat = 0
        var blue: CGFloat = 0
        var green: CGFloat = 0
        var alpha: CGFloat = 0
        uicolor.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        
        XCTAssertEqual(Int(red*10), 5)
        XCTAssertEqual(Int(green*10), 5)
        XCTAssertEqual(Int(blue*10), 5)
        XCTAssertEqual(alpha, 1)
    }
    
    @available(iOS 14.0, *)
    func testCodable() throws {
        // Test encoding
        let color = Color(red: 0.5, green: 0.3, blue: 0.7)
        let encoder = JSONEncoder()
        let decoder = JSONDecoder()
        
        let encoded = try encoder.encode(color)
        let decoded = try decoder.decode(Color.self, from: encoded)
        
        // Convert both to UIColor to compare components
        let originalComponents = color.colorComponents!
        let decodedComponents = decoded.colorComponents!
        
        XCTAssertEqual(originalComponents.red, decodedComponents.red, accuracy: 0.01)
        XCTAssertEqual(originalComponents.green, decodedComponents.green, accuracy: 0.01)
        XCTAssertEqual(originalComponents.blue, decodedComponents.blue, accuracy: 0.01)
    }
    
    @available(iOS 14.0, *)
    func testCodableWithNonRGBColor() throws {
        // Create a color using HSB, which can't be converted to RGB
        let color = Color(hue: 0.5, saturation: 0.5, brightness: 0.5)
        let encoder = JSONEncoder()
        
        // Encoding should fail silently as per implementation
        XCTAssertNoThrow(try encoder.encode(color))
    }

}
