//
//  WatchCommunication.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 10/01/2022.
//  Copyright (c) 2022 Jorge Miguel. All rights reserved.
//

import Foundation
import WatchConnectivity

public class WatchCommunication : NSObject {
    
    public typealias ResponseHandler = (_ info: [String:Any]) -> [String:Any]?
    public typealias InfoHandler = (_ info: [String:Any]) -> Void
    public typealias ErrorHandler = (_ error: Error) -> Void
    
    //--
    
    internal static let shared = WatchCommunication()
    
    // timer interval to fire the response callback for the timeout connection between the watch app and the iOS app
    private static let timerInterval: Double = 15
    
    //-- to be used when a message is received and a reply is expected
    private var responseHandler: ResponseHandler?
    
    //-- to be used when a message is received and no reply is expected
    private var infoHandler: InfoHandler?
    
    internal var queue: OperationQueue

    //--
    
    private override init() {
        self.queue = OperationQueue()
        self.queue.maxConcurrentOperationCount = 1
        self.queue.qualityOfService = .default
        self.queue.isSuspended = true //WCSession.default.activationState != .activated
        
        super.init()
        
        WCSession.default.delegate = self
        WCSession.default.activate()
    }
    
    public static func setup(infoHandler: InfoHandler? = nil, responseHandler: ResponseHandler? = nil) {
        Log.trace(ClassInfo.methodName(self), "⌚️")
        
        self.shared.infoHandler = infoHandler
        self.shared.responseHandler = responseHandler
    }
    
    deinit {
        self.queue.cancelAllOperations()
    }
}

//-- MARK: iPhone --- info ---> Watch

public extension WatchCommunication {
    
#if os(iOS)
    static func pushInfoIntoWatch(_ info: [String:Any]) {
        Log.trace(ClassInfo.methodName(self), "⌚️")
        
        guard WCSession.isSupported() && WCSession.default.isPaired &&
              WCSession.default.activationState == .activated &&
              WCSession.default.isWatchAppInstalled else {
                
            Log.debug("⌚️ info has not been pushed into watch because something is not as expected!")
            return
        }
        
        let operation = BlockOperation.init {
            try? WCSession.default.updateApplicationContext(info)
            WCSession.default.sendMessage(info, replyHandler: nil, errorHandler: nil)
        }
        
        shared.queue.addOperation(operation)
    }
#endif
    
#if os(watchOS)
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        Log.trace(ClassInfo.methodName(self), "⌚️")
        self.infoHandler?(message)
    }
    
    func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
        Log.trace(ClassInfo.methodName(self), "⌚️")
        self.infoHandler?(applicationContext)
    }
#endif
}

//-- MARK: Watch --- userInfo(FIFO) ---> iPhone

public extension WatchCommunication {
    
#if os(watchOS)
    
    static func appDidOpen() {
        guard WCSession.isSupported() else {
            return
        }
        
        //WCSession.default.transferUserInfo(["type": "app.did.open"])
    }
#endif
    
#if os(iOS)
    
    func session(_ session: WCSession, didReceiveUserInfo userInfo: [String : Any] = [:]) {
//        if userInfo["type"] == "app.did.open" {
//
//            //-- do stuff
//        }
    }
#endif
}

//-- MARK: Watch --- interactive ---> iPhone

public extension WatchCommunication {
    
#if os(watchOS)
    
    static func sendMessage(_ message: [String: Any]) {
        Log.trace(ClassInfo.methodName(self), "⌚️")
        
        WatchCommunication.sendMessage(message, replyHandler: nil, errorHandler: nil)
    }
    
    static func sendMessage(_ message: [String: Any], replyHandler: InfoHandler?, errorHandler: ErrorHandler?) {
        Log.trace(ClassInfo.methodName(self), "⌚️")
        
        guard WCSession.isSupported() else {
            Log.error(ClassInfo.methodName(self), "⌚️ WCSession is not supported")
            errorHandler?(NSError(domain: "domain.watch.communication", code: 400, userInfo: message))
            replyHandler?([:])
            return
        }
        
        // timer to send error if the apps is taking too long to comunicate
        let timer = Timer.scheduledTimer(withTimeInterval: self.timerInterval, repeats: false, block: { _ in
            errorHandler?(NSError(domain: "domain.watch.communication", code: 408, userInfo: message))
        })
        
        let operation = BlockOperation.init {
            Log.debug(ClassInfo.methodName(self), "⌚️ sending message to iOS")
            WCSession.default.sendMessage(message, replyHandler: { info in
                timer.invalidate()
                replyHandler?(info)
            }, errorHandler: { error in
                timer.invalidate()
                errorHandler?(error)
            })
        }
        
        shared.queue.addOperation(operation)
    }
#endif
    
#if os(iOS)
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        Log.trace(ClassInfo.methodName(self), "⌚️")
        self.infoHandler?(message)
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        Log.trace(ClassInfo.methodName(self), "⌚️")
        
        let response = self.responseHandler?(message)
        replyHandler(response ?? [:])
    }
#endif
}

//-- MARK: watch communication delegate

extension WatchCommunication : WCSessionDelegate {
    
    public func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        Log.trace(ClassInfo.methodName(self), "⌚️ activated: \(activationState == .activated)")
        
        if activationState == .activated {
            Log.debug(ClassInfo.methodName(self), "⌚️ resuming communication with iOS (\(self.queue.operations.count) pending)")
            self.queue.isSuspended = false
        }
    }

#if os(iOS)
    public func sessionDidBecomeInactive(_ session: WCSession) {
        Log.trace(ClassInfo.methodName(self), "⌚️")
        self.queue.isSuspended = true
    }
    
    public func sessionDidDeactivate(_ session: WCSession) {
        Log.trace(ClassInfo.methodName(self), "⌚️")
        self.queue.isSuspended = true
    }
#endif
}
