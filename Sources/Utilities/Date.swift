//
//  Date.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 16/02/2020.
//  Copyright (c) 2020 Jorge Miguel. All rights reserved.
//

import Foundation

extension Date {

    //-- MARK: date format
    
    public var isoDate: String {
        let dateFormatter = ISO8601DateFormatter()
        return dateFormatter.string(from: self)
    }
    
    public var dateTimeFormat: String {
        return self.formatted(with: "dd-MM-yyyy HH:mm:ss")
    }
    
    public func formatted(with format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: self)
    }
}
