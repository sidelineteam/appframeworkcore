//
//  DispatchQueue.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 18/05/2020.
//  Copyright (c) 2020 Jorge Miguel. All rights reserved.
//

import Foundation

public typealias DispatchQueueExecutionBlock = () -> Void

extension DispatchQueue {
    
    //-- MARK: Dispatch on Main
    
    public static func syncOnMain<T>(_ work: () throws -> T) rethrows -> T {
        if Thread.current.isMainThread {
            return try work()
            
        } else {
            return try DispatchQueue.main.sync(execute: work)
        }
    }
    
    public static func asyncOnMain(_ work: @escaping () -> Void) {
        if Thread.current.isMainThread {
            work()
        } else {
            DispatchQueue.main.async(execute: work)
        }
    }
    
    //-- MARK: Dispatch once
    
    private static var onceTracker = [String]()

    public class func once(file: String = #file, function: String = #function, line: Int = #line, block: DispatchQueueExecutionBlock) {
        
        let token = "\(file):\(function):\(line)"
        self.once(token: token, block: block)
    }

    /**
     Executes a block of code, associated with a unique token, only once.  The code is thread safe and will
     only execute the code once even in the presence of multithreaded calls.

     - parameter token: A unique reverse DNS style name such as com.vectorform.<name> or a GUID
     - parameter block: Block to execute once
     - return: true if block got executed
     */
    @discardableResult
    public class func once(token: String, block: DispatchQueueExecutionBlock) -> Bool {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }

        guard !self.onceTracker.contains(token) else { return false }

        self.onceTracker.append(token)
        block()
        
        return true
    }
    
    public class func resetOnce(token: String) {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        
        self.onceTracker.remove(object: token)
    }
}
