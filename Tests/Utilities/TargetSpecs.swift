//
//  TargetSpecs.swift
//  AppFrameworkCore
//
//  Created by Jorge Miguel on 18/02/2025.
//

import XCTest
@testable import AppFrameworkCore

final class TargetTests: XCTestCase {
    
    func testIsTestTarget() {
        // Since we're running in a test target, this should be true
        XCTAssertTrue(Target.isTestTarget)
    }
    
    func testIsDebuggingNetwork() {
        // Test without the debug flag
        XCTAssertFalse(Target.isDebuggingNetwork)
        
//        // Add the debug flag
//        let originalArgs = CommandLine.arguments
//        CommandLine.arguments.append("-network-debug")
//        
//        XCTAssertTrue(Target.isDebuggingNetwork)
//        
//        // Cleanup
//        CommandLine.arguments = originalArgs
    }
    
    func testIsiOSAppOnMac() {
        if #available(iOS 14.0, *) {
            // This will be false in the test environment
            XCTAssertFalse(Target.isiOSAppOnMac)
        } else {
            XCTAssertFalse(Target.isiOSAppOnMac)
        }
    }
    
    func testIsRunningOnCanvas() {
        // Test without the environment variable
        XCTAssertFalse(Target.isRunningOnCanvas)
        
        // Set the environment variable
        setenv("XCODE_RUNNING_FOR_PREVIEWS", "1", 1)
        XCTAssertTrue(Target.isRunningOnCanvas)
        
        // Cleanup
        unsetenv("XCODE_RUNNING_FOR_PREVIEWS")
    }
}
