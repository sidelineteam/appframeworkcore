//
//  NSLock.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 07/09/2023.
//  Copyright (c) 2023 Jorge Miguel. All rights reserved.
//

import Foundation

extension NSLock {

    @discardableResult
    public static func execute<T>(_ block: () throws -> T) rethrows -> T {
        let lock = NSLock()
        lock.lock()
        
        defer { lock.unlock() }
        
        return try block()
    }
}
