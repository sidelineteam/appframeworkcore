//
//  Configurations.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 21/06/18.
//  Copyright (c) 2018 Jorge Miguel. All rights reserved.
//

import Foundation
//import FirebaseRemoteConfig

public final class Configurations {

    public static let shared = Configurations()

    //-- static configurations encryption
    internal var staticCryptKey: String?

    fileprivate var isRuntimeConfigured = false
    fileprivate var isSharedRuntimeConfigured = false

    //-- MARK: default values plist files names

//    fileprivate let kDefaultsRemote  = "remote"
    fileprivate let kDefaultsStatic  = "static"

    //-- MARK: stores

    fileprivate lazy var runtimeConfigs = RuntimeConfigs()
    fileprivate lazy var sharedRuntimeConfigs = RuntimeConfigs(suiteName: "shared") ?? RuntimeConfigs()

    fileprivate var staticPlistPath: String?
    fileprivate lazy var staticConfigs : StaticConfiguration? = {
        if let path = self.staticPlistPath, FileManager.default.fileExists(atPath: path) {
            return StaticConfiguration(plistPath: path)
        }
        
        guard let plistPath = Bundle.main.path(forResource: kDefaultsStatic, ofType: "plist") else {
            return nil
        }

        return StaticConfiguration(plistPath: plistPath)
    }()

//    fileprivate lazy var remoteConfigs : RemoteConfig? = {
//        let configs = RemoteConfig.remoteConfig()
//        configs.setDefaults(fromPlist: kDefaultsRemote)
//
//        //-- enable developer mode
//        configs.configSettings = RemoteConfigSettings()
//
//        //-- fetch remote configs
//        configs.fetch(completionHandler: { (status, error) in
//            if status == .success {
//                configs.activate(completionHandler: nil)
//            }
//        })
//
//        return configs
//    }()

    //-- MARK: init

    init() {
        let installVersion = runtimeConfigs[RuntimeConfigs.App.kInstallationVersion]
        if installVersion.count == 0 {

            //-- current version
            guard let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {
                return
            }

            //-- set installation info
            runtimeConfigs[RuntimeConfigs.App.kInstallationVersion] = currentVersion
            runtimeConfigs[RuntimeConfigs.App.kInstallationDate] = Date()
        }
    }

    public static func config(staticPlistPath path: String? = nil, staticEncryptionKey key: String? = nil) {
        Configurations.shared.staticPlistPath = path
        Configurations.shared.staticCryptKey = key
    }

    //-- MARK: static encryption

    public static func staticConfigurationsHelper(_ configurations: [String: String]) {
        guard RuntimeConfigs.App.isProductionVersion == false else {
            return
        }

        guard let cryptKey = Configurations.shared.staticCryptKey else {
            Log.warning(ClassInfo.methodName(self), "StaticCryptKey is missing! Please configure Configurations using Configurations.config(:)")
            return
        }

        print("\n---\n| The following are the encrypted values for the static configurations you setup.\n| Please copy them to the static.plist\n|")

        for key in configurations.keys {
            print("| key: \(key) => \(configurations[key]!.encrypt(cryptKey) ?? "")")
        }

        print("---\n")
    }

    //-- MARK: Get methods

    public static func get<T>(_ config: StaticConfig<T>) -> T {
        let value = shared.staticConfigs?.get(config) ?? config.defaultValue

        if config.encrypted {
            guard let key = Configurations.shared.staticCryptKey else {
                Log.warning(ClassInfo.methodName(self), "StaticCryptKey is missing! Please configure Configurations using Configurations.config(:)")
                return value
            }

            guard let crypt = value as? String,
                  let dvalue = crypt.decryptString(key) else {
                    return value
            }

            return dvalue as? T ?? config.defaultValue
        }

        return value
    }

    public static func get<T: Codable>(_ config: RuntimeConfig<T>) -> T {
        let configs = (config.shared ? shared.sharedRuntimeConfigs : shared.runtimeConfigs)
        return configs[config] //?? config.defaultValue!
    }

    public static func get<T: Codable>(_ config: RuntimeConfig<T?>) -> T? {
        let configs = (config.shared ? shared.sharedRuntimeConfigs : shared.runtimeConfigs)
        return configs[config]
    }

//    public static func get<T>(_ key: String) -> T? {
//        return shared.remoteConfigs?[key] as? T
//    }

    //-- MARK: Set method (runtime)

    public static func set<T: Codable>(_ config: RuntimeConfig<T>, value: T) {
        let configs = (config.shared ? shared.sharedRuntimeConfigs : shared.runtimeConfigs)
        configs[config] = value
    }

    public static func set<T: Codable>(_ config: RuntimeConfig<T?>, value: T?) {
        let configs = (config.shared ? shared.sharedRuntimeConfigs : shared.runtimeConfigs)
        
        guard let value = value else {
            configs.remove(config)
            return
        }
        
        configs[config] = value
    }

    //-- MARK: Remove method (runtime)

    public static func remove<T>(_ config: RuntimeConfig<T>) {
        let configs = (config.shared ? shared.sharedRuntimeConfigs : shared.runtimeConfigs)
        configs.remove(config)
    }

}
