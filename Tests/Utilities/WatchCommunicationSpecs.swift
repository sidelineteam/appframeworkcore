//
//  WatchCommunicationSpecs.swift
//  AppFrameworkCore
//
//  Created by Jorge Miguel on 18/02/2025.
//

import XCTest
import WatchConnectivity
@testable import AppFrameworkCore

class WatchCommunicationTests: XCTestCase {
    
    var watchCommunication: WatchCommunication!
    var receivedInfo: [String: Any]?
    var receivedResponse: [String: Any]?
    
    override func setUp() {
        super.setUp()
        // Initialize WatchCommunication with test handlers
        WatchCommunication.setup(
            infoHandler: { [weak self] info in
                self?.receivedInfo = info
            },
            responseHandler: { [weak self] info in
                self?.receivedResponse = info
                return ["status": "received"]
            }
        )
    }
    
    override func tearDown() {
        watchCommunication = nil
        receivedInfo = nil
        receivedResponse = nil
        super.tearDown()
    }
    
    // Test sending message from Watch to iPhone
    #if os(watchOS)
    func testSendMessageFromWatch() {
        let expectation = XCTestExpectation(description: "Message sent from Watch")
        let testMessage = ["test": "message"]
        
        WatchCommunication.sendMessage(testMessage,
            replyHandler: { info in
                XCTAssertEqual(info["status"] as? String, "received")
                expectation.fulfill()
            },
            errorHandler: { error in
                XCTFail("Error sending message: \(error.localizedDescription)")
            }
        )
        
        wait(for: [expectation], timeout: 5.0)
    }
    #endif
    
    // Test pushing info from iPhone to Watch
    #if os(iOS)
    func testPushInfoIntoWatch() {
        let testInfo = ["test": "info"]
        WatchCommunication.pushInfoIntoWatch(testInfo)
        
        // Since this is async and depends on WCSession, we can only verify the method doesn't crash
        // Real testing would require mocking WCSession
        XCTAssertTrue(true)
    }
    
    func testReceiveMessageFromWatch() {
        let testMessage = ["test": "message"]
        let session = WCSession.default
        
        // Simulate receiving a message
        session.delegate?.session?(session, didReceiveMessage: testMessage)
        
        XCTAssertEqual(receivedInfo?["test"] as? String, "message")
    }
    #endif
    
    // Test activation state changes
    func testActivationStateChange() {
        let session = WCSession.default
        
        // Test activation completion
        session.delegate?.session(
            session,
            activationDidCompleteWith: .activated,
            error: nil
        )
        
        // Verify queue is not suspended when activated
        XCTAssertFalse(WatchCommunication.shared.queue.isSuspended)
        
        #if os(iOS)
        // Test deactivation
        session.delegate?.sessionDidDeactivate(session)
        
        // Verify queue is suspended when deactivated
        XCTAssertTrue(WatchCommunication.shared.queue.isSuspended)
        #endif
    }
}
