//
//  AppUpgrade.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 28/06/18.
//  Copyright (c) 2018 Jorge Miguel. All rights reserved.
//

import Foundation

public typealias AppUpgradeCallback = (_ upgraded: Bool, _ previousVersion: UInt) -> Void

public final class AppUpgrade {

//    Usage example:
//
//    public static func validate() {
//        self.validateAppVersionUpgrade { (upgraded, prevVersion) in
//            if upgraded {
//                if prevVersion < kUpdateVersion_1_0_0 {
//                    //-- do stuff...
//                }
//                if prevVersion < kUpdateVersion_1_x_x {
//                    //-- do stuff...
//                }
//                //--- ...
//            }
//        }
//    }

    //-- MARK: App upgrade validation

    public static func validateAppVersionUpgrade(_ handler: AppUpgradeCallback) {
        let version: Int = RuntimeConfigs.App.currentVersion

        //-- current version
        guard let currentVersionStr = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String,
              let currentVersion = Int(currentVersionStr.replacingOccurrences(of: ".", with: "")), currentVersion > 0 else {

                Analytics.error("\(ClassInfo.methodName(self)): Error processing app version.")
                handler(false, UInt(version))
                return
        }

        let firstRun = RuntimeConfigs.App.isFirstRun
        let upgraded = firstRun == false && version < currentVersion

        //-- TODO: Analytics
        if upgraded {
            let info = ["From": "\(version)",
                        "To": "\(currentVersion)",
                        "From - To": "\(version) - \(currentVersion)"]

            Analytics.checkpoint("App Upgraded", properties: info)

            //-- update configuration values
            RuntimeConfigs.App.currentVersion = currentVersion
            RuntimeConfigs.App.currentVersionString = currentVersionStr
        }

        handler(upgraded, UInt(version))
    }

}
