//
//  RuntimeConfigsSpecs.swift
//  AppFrameworkCore
//
//  Created by Jorge Miguel on 18/02/2025.
//

import XCTest
@testable import AppFrameworkCore

class RuntimeConfigsSpecs: XCTestCase {
    
    var configs: RuntimeConfigs!
    
    override func setUp() {
        super.setUp()
        // Using a test suite name to avoid interfering with actual app settings
        configs = RuntimeConfigs(suiteName: "com.test.runtimeconfigs")
    }
    
    override func tearDown() {
        // Clean up user defaults after each test
        UserDefaults.standard.removePersistentDomain(forName: "com.test.runtimeconfigs")
        configs = nil
        super.tearDown()
    }
    
    func testBasicConfiguration() {
        // Test basic string configuration
        let stringConfig = RuntimeConfig<String>("test.string", defaultValue: "default")
        configs.set(stringConfig, value: "test value")
        XCTAssertEqual(configs.get(stringConfig), "test value")
        
        // Test basic integer configuration
        let intConfig = RuntimeConfig<Int>("test.int", defaultValue: 0)
        configs.set(intConfig, value: 42)
        XCTAssertEqual(configs.get(intConfig), 42)
        
        // Test basic boolean configuration
        let boolConfig = RuntimeConfig<Bool>("test.bool", defaultValue: false)
        configs.set(boolConfig, value: true)
        XCTAssertEqual(configs.get(boolConfig), true)
    }
    
    func testOptionalConfiguration() {
        let optionalConfig = RuntimeConfig<String?>("test.optional", defaultValue: nil)
        
        // Test nil value
        XCTAssertNil(configs.get(optionalConfig))
        
        // Test setting and getting value
        configs.set(optionalConfig, value: "optional value")
        XCTAssertEqual(configs.get(optionalConfig), "optional value")
        
        // Test removing value
        configs.remove(optionalConfig)
        XCTAssertNil(configs.get(optionalConfig))
    }
    
    func testEncryptedConfiguration() {
        let encryptedConfig = RuntimeConfig<String>("test.encrypted",
                                                  defaultValue: "default",
                                                  encrypted: true)
        
        configs.set(encryptedConfig, value: "secret value")
        XCTAssertEqual(configs.get(encryptedConfig), "secret value")
        
        // Verify the stored value is actually encrypted
        let rawValue = configs.value(forKey: encryptedConfig.key.sha256)
        XCTAssertNotNil(rawValue)
        XCTAssertTrue(rawValue is Data)
        XCTAssertNotEqual(String(data: rawValue as! Data, encoding: .utf8), "secret value")
    }
    
    func testCodableConfiguration() {
        struct TestCodable: Codable, Equatable {
            let name: String
            let value: Int
        }
        
        let codableConfig = RuntimeConfig<TestCodable>(
            "test.codable",
            defaultValue: TestCodable(name: "default", value: 0),
            codable: true
        )
        
        let testValue = TestCodable(name: "test", value: 42)
        configs.set(codableConfig, value: testValue)
        
        let retrievedValue = configs.get(codableConfig)
        XCTAssertEqual(retrievedValue, testValue)
    }
    
    func testDefaultValues() {
        let config = RuntimeConfig<String>("test.defaults", defaultValue: "default value")
        XCTAssertEqual(configs.get(config), "default value")
    }
    
    func testAppConfiguration() {
        // Test enableLogs
        RuntimeConfigs.App.enableLogs = true
        XCTAssertTrue(RuntimeConfigs.App.enableLogs)
        
        // Test version configurations
        RuntimeConfigs.App.currentVersion = 200
        XCTAssertEqual(RuntimeConfigs.App.currentVersion, 200)
        
        RuntimeConfigs.App.currentVersionString = "2.0.0"
        XCTAssertEqual(RuntimeConfigs.App.currentVersionString, "2.0.0")
        
        // Test installation configurations
        let testDate = Date()
        RuntimeConfigs.App.installationDate = testDate
        XCTAssertEqual(RuntimeConfigs.App.installationDate, testDate)
        
        RuntimeConfigs.App.installationVersion = "1.0.0"
        XCTAssertEqual(RuntimeConfigs.App.installationVersion, "1.0.0")
        
        // Test boolean flags
        RuntimeConfigs.App.isFirstRun = false
        XCTAssertFalse(RuntimeConfigs.App.isFirstRun)
        
        RuntimeConfigs.App.isProVersion = true
        XCTAssertTrue(RuntimeConfigs.App.isProVersion)
    }
}
