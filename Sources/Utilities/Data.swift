//
//  Data.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 17/07/2018.
//  Copyright (c) 2018 Jorge Miguel. All rights reserved.
//

import CryptoKit
import Foundation
import CommonCrypto

extension Data {
    
    //-- MARK: convertion
    
    public var utf8String: String? {
        String(data: self, encoding: .utf8)
    }
    
    //-- MARK: hashing

    public var md5: [UInt8] {
        Array(Insecure.MD5.hash(data: self))
    }
    
    public var sha256: [UInt8] {
        var hash = [UInt8](repeating: 0, count: Int(CC_SHA256_DIGEST_LENGTH))
        _ = self.withUnsafeBytes {
            CC_SHA256($0.baseAddress, CC_LONG(self.count), &hash)
        }
        return hash
    }
    
    //-- MARK: random
    
    public static func random(_ size: Int) -> Data {
        let content: [UInt8] = (1...size).map { _ in UInt8.random(in: 0...255) }
        return Data(content)
    }

    //-- MARK: encryption

    public func encrypt(_ salt: String) -> Data? {
        guard let data = salt.data(using: .utf8) else {
            Log.error(ClassInfo.methodName(self), "salt is not compatible!")
            return nil
        }
        
        do {
            let nonce = AES.GCM.Nonce()
            let saltData = SHA256.hash(data: data)
            let key = SymmetricKey(data: saltData)
            let sealedBox = try AES.GCM.seal(self, using: key, nonce: nonce)
            return sealedBox.combined
            
        } catch {
            Log.error(ClassInfo.methodName(self), error.localizedDescription)
        }

        return nil
    }

    public func decrypt(_ salt: String) -> Any? {
        guard let data = salt.data(using: .utf8) else {
            Log.error(ClassInfo.methodName(self), "salt is not compatible!")
            return nil
        }
        
        do {
            let saltData = SHA256.hash(data: data)
            let key = SymmetricKey(data: saltData)
            let sealedBox = try AES.GCM.SealedBox(combined: self)
            return try AES.GCM.open(sealedBox, using: key)

        } catch {
            Log.error(ClassInfo.methodName(self), error.localizedDescription)
        }

        return nil
    }

    //-- MARK: archive (Codable)

    public static func encode<T: Encodable>(_ value: T, secure: Bool = true) throws -> Data {
        let data = try PropertyListEncoder().encode(value)
        return try NSKeyedArchiver.archivedData(withRootObject: data, requiringSecureCoding: secure)
    }

    public func decode<T: Decodable>() throws -> T? {
        guard let data = try NSKeyedUnarchiver.unarchivedObject(ofClass: NSData.self, from: self) as? Data else {
            return nil
        }

        return try PropertyListDecoder().decode(T.self, from: data)
    }

    public func decode<T: Decodable>(_ type: T.Type) throws -> T? {
        guard let data = try NSKeyedUnarchiver.unarchivedObject(ofClass: NSData.self, from: self) as? Data else {
            return nil
        }

        return try PropertyListDecoder().decode(type, from: data)
    }

    //-- MARK: archive

    public static func archive<T>(_ value: T, secure: Bool = true) throws -> Data {
        return try NSKeyedArchiver.archivedData(withRootObject: value, requiringSecureCoding: secure)
    }

    public func unarchive<T>() -> T? {
        NSKeyedUnarchiver.unarchiveObject(with: self) as? T
    }

    //-- MARK: handling

    public func subdata(in range: ClosedRange<Index>) -> Data {
        return subdata(in: range.lowerBound ..< range.upperBound + 1)
    }
    
    public func split(size: Int, handler: @escaping ((Data) -> Void)) {
        let totalSize = count
        var offset = 0
        
        while offset < totalSize {
            let chunkSize = Swift.min(size, totalSize - offset)
            let chunk = self[offset..<offset + chunkSize]
            handler(chunk)
            offset += chunkSize
        }
    }
}
