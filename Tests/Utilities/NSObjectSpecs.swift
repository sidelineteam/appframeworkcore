//
//  NSObjectSpecs.swift
//  AppFrameworkCore
//
//  Created by Jorge Miguel on 18/02/2025.
//

import XCTest
@testable import AppFrameworkCore

class NSObjectSpecs: XCTestCase {
    
    private var testObject: NSObject!
    private var lockObject: NSObject!
    
    override func setUp() {
        super.setUp()
        testObject = NSObject()
        lockObject = NSObject()
    }
    
    override func tearDown() {
        testObject = nil
        lockObject = nil
        super.tearDown()
    }
    
    func testSynchronizedSuccess() {
        // Given
        let expectedValue = 42
        
        // When
        let result = testObject.synchronized(lockObject) {
            return expectedValue
        }
        
        // Then
        XCTAssertEqual(result, expectedValue)
    }
    
    func testSynchronizedWithError() {
        // Given
        struct TestError: Error {}
        
        // When/Then
        XCTAssertThrowsError(try testObject.synchronized(lockObject) {
            throw TestError()
        })
    }
    
    func testSynchronizedConcurrency() {
        // Given
        let expectation = XCTestExpectation(description: "Concurrent access")
        var sharedCounter = 0
        let iterations = 1000
        let queue = DispatchQueue(label: "com.test.queue", attributes: .concurrent)
        
        // When
        for _ in 0..<iterations {
            queue.async {
                self.testObject.synchronized(self.lockObject) {
                    sharedCounter += 1
                    if sharedCounter == iterations {
                        expectation.fulfill()
                    }
                }
            }
        }
        
        // Then
        wait(for: [expectation], timeout: 5.0)
        XCTAssertEqual(sharedCounter, iterations)
    }
}
