//
//  Utilities.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 11/07/18.
//  Copyright (c) 2018 Jorge Miguel. All rights reserved.
//

import Foundation

public extension NSObject {

    var className: String {
        let name = NSStringFromClass(type(of: self))
        guard let idx = name.firstIndex(of: ".") else {
            return name
        }

        return String(name.suffix(from: name.index(idx, offsetBy: 1)))
    }

    func methodName(_ f: String = #function) -> String {
        return "\(self.className):\(f)"
    }
}

open class ClassInfo {
    
    public init() { }

    public var className: String {
        guard RuntimeConfigs.App.enableLogs else {
            return ""
        }
        
        return ClassInfo.className(self)
    }

    public func methodName(_ f: String = #function) -> String {
        guard RuntimeConfigs.App.enableLogs else {
            return ""
        }
        
        return "\(self.className).\(f)"
    }

    public class func className(_ obj: Any) -> String {
        guard RuntimeConfigs.App.enableLogs else {
            return ""
        }
        
        let name = String(describing: obj.self)
        guard name.hasPrefix("<"), let idx = name.firstIndex(of: ".") else {
            let ignore = (obj as? Decodable == nil)
            return (ignore ? name : "DecodableObj")
        }

        return String(name.suffix(from: name.index(idx, offsetBy: 1)))
    }

    public class func methodName(_ obj: Any, f: String = #function) -> String {
        guard RuntimeConfigs.App.enableLogs else {
            return ""
        }
        
        return "\(ClassInfo.className(obj)).\(f)"
    }
}
