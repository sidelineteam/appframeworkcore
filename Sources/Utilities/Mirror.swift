//
//  Mirror.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 05/06/2021.
//  Copyright (c) 2021 Jorge Miguel. All rights reserved.
//

import Foundation

public extension Mirror {

    typealias PropertyHandler<T> = (_ name: String, _ value: T) -> Bool
    
    static func reflectProperties<T>(of target: Any, using handler: PropertyHandler<T>) {
        let mirror = Mirror(reflecting: target)
        for child in mirror.children {
            guard let value = (child.value as? T) else {
                continue
            }
            
            if handler(child.label ?? "<unknown>", value) {
              break
            }
        }
    }
}
