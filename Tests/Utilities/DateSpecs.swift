//
//  DateSpecs.swift
//  AppFrameworkTests
//
//  Created by Jorge Miguel on 27/04/2020.
//  Copyright © 2020 JMIguel. All rights reserved.
//

import XCTest

@testable import AppFrameworkCore

class DateSpecs: XCTestCase {
    
        func testIsoDate() {
            let date = Date(timeIntervalSince1970: 0)
            let dateStr: String = date.isoDate
            XCTAssertEqual(dateStr, "1970-01-01T00:00:00Z")
        }
    
        func testDateTimeFormat() {
            let date = Date(timeIntervalSince1970: 0)
            let dateStr: String = date.dateTimeFormat
            XCTAssertEqual(dateStr, "01-01-1970 01:00:00")
        }
    
    func testFormattedWithCustomFormat() {
        let date = Date(timeIntervalSince1970: 0)
        let dateStr = date.formatted(with: "yyyy/MM/dd")
        XCTAssertEqual(dateStr, "1970/01/01")
        
        // Test with different format
        let timeStr = date.formatted(with: "HH:mm")
        XCTAssertEqual(timeStr, "01:00")
    }
}
