//
//  StaticConfigs.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 21/06/18.
//  Copyright (c) 2018 Jorge Miguel. All rights reserved.
//

import Foundation

open class StaticConfigs {

    struct Analytics {
        
        struct Provider {
            
            private static let kCrahslyticsEnabled  = StaticConfig<Bool>("appframework.analytics.provider.crashlytics", defaultValue: false)
            private static let kFirebaseEnabled     = StaticConfig<Bool>("appframework.analytics.provider.firebase", defaultValue: false)

            internal static var crahslyticsEnabled: Bool  { Configurations.get(kCrahslyticsEnabled) }
            internal static var firebaseEnabled: Bool     { Configurations.get(kFirebaseEnabled) }
        }
    }

    public struct App {
        
        private static let kName = StaticConfig<String>("app.name", defaultValue: "{APP_NAME}")
        private static let kBundleId = StaticConfig<String>("app.bundle_id", defaultValue: "{APP_BUNDLE_ID}")

        public static var name: String { Configurations.get(kName) }
        public static var bundleId: String { Configurations.get(kBundleId) }
    }

    public struct Feedback {
        
        private static let kEmailAddress = StaticConfig<String>("appframework.feedback.email_address", defaultValue: "")
        private static let kKey = StaticConfig<String>("appframework.feedback.key", defaultValue: "", encrypted: true) //-- key for the SendGrid service

        internal static var emailAddress: String { Configurations.get(kEmailAddress) }
        internal static var key: String          { Configurations.get(kKey) }
    }
}

//-------------------------------------------------------------------------- IMPLEMENTATION

// Inspired by SwiftyConfiguration (https://github.com/ykyouhei/SwiftyConfiguration)

public protocol PlistValueType {}

extension String: PlistValueType {}
extension URL: PlistValueType {}
extension NSNumber: PlistValueType {}
extension Int: PlistValueType {}
extension Float: PlistValueType {}
extension Double: PlistValueType {}
extension Bool: PlistValueType {}
extension Date: PlistValueType {}
extension Data: PlistValueType {}
extension Array: PlistValueType {}
extension Dictionary: PlistValueType {}

//--------------------------------------------------------------------------

public final class StaticConfig<ValueType: PlistValueType> : StaticConfigs {

    internal let key: String
    internal let encrypted: Bool
    
    public let defaultValue: ValueType

    internal var separatedKeys: [String] {
        return key.components(separatedBy: ".")
    }

    public init(_ key: String, defaultValue: ValueType, encrypted: Bool = false) {
        self.key = key
        self.encrypted = encrypted
        self.defaultValue = defaultValue
    }

}

//--------------------------------------------------------------------------

public struct StaticConfiguration {

    private let configs: Any

    public init?(plistPath: String) {
        guard let plist = NSDictionary(contentsOfFile: plistPath) else {
            Log.error("AppFrameworkV2.StaticConfiguration.init", "Could not read plist file.")
            return nil
        }

        self.configs = plist
    }

    public func get<T>(_ key: StaticConfig<T>) -> T {
        var configVal: Any? = self.configs

        key.separatedKeys.enumerated().forEach { idx, separatedKey in
            if let index = Int(separatedKey) {
                guard let array = configVal as? [Any] else {
                    //Log.error(ClassInfo.methodName(self), "Error reading static configuration '\(separatedKey)'")
                    configVal = nil
                    return
                }

                configVal = array[index]

            } else {
                guard let dictionary = configVal as? [String : Any], let val = dictionary[separatedKey] else {
                    //Log.error(ClassInfo.methodName(self), "Error reading static configuration '\(separatedKey)'")
                    configVal = nil
                    return
                }

                configVal = val
            }
        }

        guard let value = configVal else {
            return key.defaultValue
        }

        let optionalValue: T?

        switch T.self {
        case is Int.Type:
            optionalValue = value as? T
        case is Float.Type:
            optionalValue = Float(value as! String) as? T
        case is Double.Type:
            optionalValue = Double(value as! String) as? T
        case is URL.Type:
            optionalValue = URL(string: value as! String) as? T
        default:
            optionalValue = value as? T
        }

        guard let v = optionalValue else {
            Log.error(ClassInfo.methodName(self), "Could not cast value of type \(type(of: value)) to \(T.self)")
            return key.defaultValue
        }

        return v
    }

}
