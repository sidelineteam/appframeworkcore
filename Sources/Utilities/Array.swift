//
//  Array.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 23/07/2018.
//  Copyright (c) 2018 Jorge Miguel. All rights reserved.
//

import Foundation

extension Array {

    public func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
    
    //-- MARK: JSON

    public var jsonData: Data? {
        return try? JSONSerialization.data(withJSONObject: self, options: [])
    }
    
    public var json: String? {
        guard let data = self.jsonData else {
            return nil
        }

        return String(data: data, encoding: String.Encoding.utf8)
    }
}

extension Array where Element: Hashable {
    
    public func differences(from other: [Element]) -> [Element] {
        let thisSet = Set(self)
        let otherSet = Set(other)
        return Array(thisSet.symmetricDifference(otherSet))
    }
    
    public var removeDuplicates: [Element] {
        var seen = Set<Element>()
        return filter{ seen.insert($0).inserted }
    }
}

extension Array where Element: Equatable {

    //-- MARK: remove(obj:)

    @discardableResult
    public mutating func remove(object: Element) -> Bool {
        let count = self.count
        self.removeAll(where: { $0 == object })
        return count > self.count
    }
    
    //-- MARK: Diff
    
    public mutating func update(with subarray: [Element]) {
        guard self.isEmpty == false else {
            self = subarray
            return
        }

        guard subarray.isEmpty == false else {
            self.removeAll()
            return
        }
        
        let first = subarray.first(where: { self.contains($0) })
        let last = subarray.last(where: { self.contains($0) })

        guard let first = first, let last = last else {
            self.append(contentsOf: subarray)
            return
        }
        
        guard let first = self.firstIndex(of: first),
              let last = self.firstIndex(of: last) else {
            
            self.append(contentsOf: subarray)
            return
        }
        
        let sArr = Array(self.prefix(upTo: last+1).suffix(from: first))
        let diff = subarray.difference(from: sArr, by: ==)
            
        diff.forEach { change in
            switch change {
            case .remove(offset: let offset, element: _, associatedWith: _):
                self.remove(at: first + offset)
            case .insert(offset: let offset, element: let element, associatedWith: _):
                self.insert(element, at: first + offset)
            }
        }
    }
}
    
extension Array where Element: Identifiable {
    
    @discardableResult
    public mutating func replace(_ object: Element) -> Int? {
        guard let pos = (self.firstIndex { $0.id == object.id }) else {
            return nil
        }
        
        self[pos] = object
        return pos
    }
}
