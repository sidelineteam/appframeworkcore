//
//  ClassInfoSpecs.swift
//  AppFrameworkCore
//
//  Created by Jorge Miguel on 18/02/2025.
//

import XCTest
import AppFrameworkCore

class ClassInfoSpecs: XCTestCase {
    
    override func setUp() {
        super.setUp()
        RuntimeConfigs.App.enableLogs = true
    }
    
    override func tearDown() {
        RuntimeConfigs.App.enableLogs = false
        super.tearDown()
    }
    
    func testClassName() {
        // Test instance className
        let testClass = TestClass()
        XCTAssertEqual(testClass.className.split(separator: ".").last, "TestClass")
        
        // Test static className with regular object
        XCTAssertEqual(ClassInfo.className(testClass).split(separator: ".").last, "TestClass")
        
        // Test with Decodable object
        let decodableClass = TestDecodableClass()
        XCTAssertEqual(ClassInfo.className(decodableClass), "DecodableObj")
        
        // Test with logs disabled
        RuntimeConfigs.App.enableLogs = false
        XCTAssertEqual(testClass.className, "")
        XCTAssertEqual(ClassInfo.className(testClass), "")
    }
    
    func testMethodName() {
        // Test instance methodName
        let testClass = TestClass()
        XCTAssertTrue(testClass.methodName().hasSuffix("TestClass.testMethodName()"))
        XCTAssertTrue(testClass.methodName("customMethod").hasSuffix("TestClass.customMethod"))
        
        // Test static methodName
        XCTAssertTrue(ClassInfo.methodName(testClass).hasSuffix("TestClass.testMethodName()"))
        XCTAssertTrue(ClassInfo.methodName(testClass, f: "customMethod").hasSuffix("TestClass.customMethod"))
        
        // Test with logs disabled
        RuntimeConfigs.App.enableLogs = false
        XCTAssertEqual(testClass.methodName(), "")
        XCTAssertEqual(ClassInfo.methodName(testClass), "")
    }
}

// Test helper classes
fileprivate class TestClass: ClassInfo { }

fileprivate class TestDecodableClass: ClassInfo, Decodable {
    
    override init() { }
    
    required init(from decoder: any Decoder) throws { }
}
