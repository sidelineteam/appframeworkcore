//
//  ConfigurationsSpecs.swift
//  AppFrameworkCore
//
//  Created by Jorge Miguel on 18/02/2025.
//

import XCTest
@testable import AppFrameworkCore

class ConfigurationsSpecs: XCTestCase {
    
    var sut: Configurations!
    
    override func setUp() {
        super.setUp()
        sut = Configurations.shared
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    // MARK: - Initialization Tests
    
    func testInitializationSetsInstallationVersion() {
        // Given
        let bundleVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        // When
        let installVersion = Configurations.get(RuntimeConfigs.App.kInstallationVersion)
        
        // Then
        XCTAssertEqual(installVersion, bundleVersion)
    }
    
    // MARK: - Static Configuration Tests
    
    func testStaticConfigEncryption() {
        // Given
        let testKey = "testEncryptionKey"
        let testConfigs = ["key1": "value1", "key2": "value2"]
        
        // When
        Configurations.config(staticEncryptionKey: testKey)
        Configurations.staticConfigurationsHelper(testConfigs)
        
        // Then
        XCTAssertNotNil(Configurations.shared.staticCryptKey)
        XCTAssertEqual(Configurations.shared.staticCryptKey, testKey)
    }
    
    func testStaticConfigGet() {
        // Given
        let testKey = "testKey"
        let defaultValue = "defaultValue"
        let config = StaticConfig(testKey, defaultValue: defaultValue)
        
        // When
        let value = Configurations.get(config)
        
        // Then
        XCTAssertEqual(value, defaultValue)
    }
    
    // MARK: - Runtime Configuration Tests
    
    func testRuntimeConfigSetAndGet() {
        // Given
        let config = RuntimeConfig("testKey", defaultValue: "testValue")
        let newValue = "newValue"
        
        // When
        Configurations.set(config, value: newValue)
        let retrievedValue = Configurations.get(config)
        
        // Then
        XCTAssertEqual(retrievedValue, newValue)
    }
    
    func testRuntimeConfigRemove() {
        // Given
        let config = RuntimeConfig("testKey", defaultValue: "testValue")
        Configurations.set(config, value: "someValue")
        
        // When
        Configurations.remove(config)
        let retrievedValue = Configurations.get(config)
        
        // Then
        XCTAssertEqual(retrievedValue, config.defaultValue)
    }
    
    func testOptionalRuntimeConfig() {
        // Given
        let config = RuntimeConfig<String?>("optionalKey", defaultValue: nil)
        let testValue = "testValue"
        
        // When
        Configurations.set(config, value: testValue)
        let retrievedValue = Configurations.get(config)
        
        // Then
        XCTAssertEqual(retrievedValue, testValue)
        
        // When removing
        Configurations.set(config, value: nil)
        let nilValue = Configurations.get(config)
        
        // Then
        XCTAssertNil(nilValue)
    }
    
    func testSharedRuntimeConfig() {
        // Given
        let config = RuntimeConfig("sharedKey", defaultValue: "defaultValue", shared: true)
        let sharedValue = "sharedValue"
        
        // When
        Configurations.set(config, value: sharedValue)
        let retrievedValue = Configurations.get(config)
        
        // Then
        XCTAssertEqual(retrievedValue, sharedValue)
    }
}
