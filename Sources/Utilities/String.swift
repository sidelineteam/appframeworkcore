//
//  String.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 17/07/2018.
//  Copyright (c) 2018 Jorge Miguel. All rights reserved.
//

import UIKit
import SwiftUI
import Foundation
import CommonCrypto

extension String {

    //-- MARK: syntax candy

    public var url: URL? { URL(string: self) }

    public var image: UIImage? { UIImage(named: self) }
    
    public var urlQueryEncoded: String? { self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) }
    
    public var isEmptyOrWhiteSpaces: Bool { self.isEmpty || self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty }

    //-- MARK: syntax candy: Swift UI
    
    public var color: Color { Color(hex: self) }

    public func font(_ size: CGFloat) -> Font { Font.custom(self, size: size) }

    //-- MARK: syntax candy: localization
    
    public var localized: String { NSLocalizedString(self, comment: "") }

    public func localized(_ fileName: String = "Localizable-brand") -> String { NSLocalizedString(self, tableName: fileName, bundle: Bundle.main, value: self.localized, comment: "") }

    public var localizedUppercase: String { NSLocalizedString(self, comment: "").uppercased() }

    public func localizedUppercase(_ fileName: String = "Localizable-brand") ->  String { NSLocalizedString(self, tableName: fileName, bundle: Bundle.main, value: self.localized, comment: "").uppercased() }

    public var localizedCapitalized: String { NSLocalizedString(self, comment: "").capitalized }
    
    public func localizedCapitalized(_ fileName: String = "Localizable-brand") ->  String { NSLocalizedString(self, tableName: fileName, bundle: Bundle.main, value: self.localized, comment: "").capitalized }
    
    public func localized(_ elements: [String:String]? = nil, count: Int? = nil, fileName: String = "Localizable-brand") -> String {
        
        //-- cardinality
        let suffix = (count == 1 ? "_1" : (count != nil ? "_n" : ""))
        
        //-- localization
        var str = self.appending(suffix).localized(fileName)
        
        //-- replacements
        guard let elements = elements else {
            return str
        }
        
        elements.forEach { key, value in
            str = str.replacingOccurrences(of: "${\(key)}", with: value)
        }
        
        return str
    }
    
    //-- MARK: JSON
    
    public var jsonDictionary: [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                
            } catch {
                Log.error(ClassInfo.methodName(self), error.localizedDescription)
            }
        }
        
        return nil
    }

    //-- MARK: hashing

    public var sha256: String {
        let context = UnsafeMutablePointer<CC_SHA256_CTX>.allocate(capacity: 1)
        var digest = Array<UInt8>(repeating:0, count:Int(CC_SHA256_DIGEST_LENGTH))
        CC_SHA256_Init(context)
        CC_SHA256_Update(context, self, CC_LONG(self.lengthOfBytes(using: String.Encoding.utf8)))
        CC_SHA256_Final(&digest, context)
        context.deallocate()
        var hexString = ""
        for byte in digest {
            hexString += String(format:"%02x", byte)
        }

        return hexString
    }

    //-- MARK: encryption

    public func encrypt(_ salt: String) -> String? {
        guard let data = data(using: .utf8) else {
            return nil
        }

        return data.encrypt(salt)?.base64EncodedString()
    }

    public func decrypt(_ salt: String) -> Any? {
        let data = Data(base64Encoded: self)
        return data?.decrypt(salt)
    }

    public func decryptString(_ salt: String) -> String? {
        guard let data = self.decrypt(salt) as? Data else {
            return nil
        }

        return String(data: data, encoding: .utf8)
    }
    
    //-- MARK: text format
    
    public var initials: String {
        let formatter = PersonNameComponentsFormatter()
        guard let components = formatter.personNameComponents(from: self) else {
             return self
        }
        
        formatter.style = .abbreviated
        return formatter.string(from: components)
    }
    
    public func removePrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) else {
            return self
        }
        
        return String(self.dropFirst(prefix.count))
    }

    //-- MARK: date format

    public var isoDate: Date? {
        let dateFormatter = ISO8601DateFormatter()
        return dateFormatter.date(from: self)
    }

    public func date(_ format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self)
    }
    
    //-- MARK: generators
    
    public static func random(length: Int = 20) -> String {
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: String = ""

        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        
        return randomString
    }

    //-- MARK: text validation
    
    public var isValidEmail: Bool {
        let emailRegEx = "[A-Za-z0-9_%+-]+(?:\\.[A-Za-z0-9_%+-]+)*@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    public var isValidUrl: Bool {
        let urlRegEx = "((http|https)://)(www.)?[a-zA-Z0-9@:%._\\+~#?&//=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)"
        let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
        return urlTest.evaluate(with: self)
    }

    //-- MARK: regex

    public func match(_ regex: String) -> [[String]] {
        guard let regex = try? NSRegularExpression(pattern: regex, options: []) else {
            return []
        }

        let nsString = self as NSString
        let results  = regex.matches(in: self, options: [], range: NSMakeRange(0, nsString.length))

        return results.map { result in
            (0..<result.numberOfRanges).map {
                result.range(at: $0).location != NSNotFound
                    ? nsString.substring(with: result.range(at: $0))
                    : ""
            }
        }
    }
    
    //-- MARK: encoding
    
    public var encodeToBase64: String? {
        self.data(using: .utf8)?.base64EncodedString()
    }
    
    //-- MARK: JWT Decoder
    
    public var jwtTokenDictionary : [String : Any] {
        get throws {
            
            enum DecodeErrors: Error {
                case badToken
                case other
            }
            
            func base64Decode(_ base64: String) throws -> Data {
                let base64 = base64
                    .replacingOccurrences(of: "-", with: "+")
                    .replacingOccurrences(of: "_", with: "/")
                let padded = base64.padding(toLength: ((base64.count + 3) / 4) * 4, withPad: "=", startingAt: 0)
                guard let decoded = Data(base64Encoded: padded) else {
                    throw DecodeErrors.badToken
                }
                return decoded
            }
            
            func decodeJWTPart(_ value: String) throws -> [String: Any] {
                let bodyData = try base64Decode(value)
                let json = try JSONSerialization.jsonObject(with: bodyData, options: [])
                guard let payload = json as? [String: Any] else {
                    throw DecodeErrors.other
                }
                return payload
            }
            
            let segments = self.components(separatedBy: ".")
            return try decodeJWTPart(segments[1])
        }
    }
}
