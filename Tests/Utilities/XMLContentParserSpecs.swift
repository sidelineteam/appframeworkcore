//
//  XMLContentParserSpecs.swift
//  AppFrameworkTests
//
//  Created by Jorge Miguel on 24/03/2021.
//  Copyright © 2021 JMIguel. All rights reserved.
//

import XCTest

@testable import AppFrameworkCore

class XMLContentParserSpecs: XCTestCase {

    func testContentParser() {
        let xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><root configId=\"2975768\" xmlns=\"urn:schemas-upnp-org:device-1-0\" xmlns:dlna=\"urn:schemas-dlna-org:device-1-0\"><device><deviceType>urn:schemas-upnp-org:device:MediaRenderer:1</deviceType><friendlyName>Kodi (LibreELEC)</friendlyName><manufacturer>XBMC Foundation</manufacturer><manufacturerURL>http://kodi.tv/</manufacturerURL><modelDescription>Kodi - Media Renderer</modelDescription><modelName>Kodi</modelName><modelNumber>18.9 (18.9.0) Git:newclock5_18.9-Leia</modelNumber><modelURL>http://kodi.tv/</modelURL><UDN>uuid:2b003f0d-a66e-dde3-c683-9b80f9b16d98</UDN><presentationURL>http://192.168.1.119:8080/</presentationURL><dlna:X_DLNADOC xmlns:dlna=\"urn:schemas-dlna-org:device-1-0\">DMR-1.50</dlna:X_DLNADOC></device></root>"
        let parser = XMLParser(data: xml.data(using: .utf8)!)
        
        let exp = self.expectation(description: "Starting Test: \(#function)")
        
        _ = XMLContentParser(with: parser, find: ["deviceType", "thisWillNotBeFound", "friendlyName"]) { content in
            XCTAssertEqual(content.count, 2)
            exp.fulfill()
        }
        
        self.waitForExpectations(timeout: 10) { error in
            XCTAssert(error == nil, "Test Failed: \(#function) | error: \(error?.localizedDescription ?? "none")")
        }
    }
    
    func testEmptyXMLContent() {
        let xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><root></root>"
        let parser = XMLParser(data: xml.data(using: .utf8)!)
        
        let exp = self.expectation(description: "Starting Test: \(#function)")
        
        _ = XMLContentParser(with: parser, find: ["deviceType", "friendlyName"]) { content in
            XCTAssertEqual(content.count, 0)
            exp.fulfill()
        }
        
        self.waitForExpectations(timeout: 10) { error in
            XCTAssert(error == nil, "Test Failed: \(#function) | error: \(error?.localizedDescription ?? "none")")
        }
    }
    
    func testPartialElementsFound() {
        let xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><root><deviceType>test-device</deviceType></root>"
        let parser = XMLParser(data: xml.data(using: .utf8)!)
        
        let exp = self.expectation(description: "Starting Test: \(#function)")
        
        _ = XMLContentParser(with: parser, find: ["deviceType", "friendlyName"]) { content in
            XCTAssertEqual(content.count, 1)
            XCTAssertEqual(content["devicetype"], "test-device")
            exp.fulfill()
        }
        
        self.waitForExpectations(timeout: 10) { error in
            XCTAssert(error == nil, "Test Failed: \(#function) | error: \(error?.localizedDescription ?? "none")")
        }
    }
    
    func testCaseInsensitiveElementMatching() {
        let xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><root><DeviceType>test-device</DeviceType></root>"
        let parser = XMLParser(data: xml.data(using: .utf8)!)
        
        let exp = self.expectation(description: "Starting Test: \(#function)")
        
        _ = XMLContentParser(with: parser, find: ["devicetype"]) { content in
            XCTAssertEqual(content.count, 1)
            XCTAssertEqual(content["devicetype"], "test-device")
            exp.fulfill()
        }
        
        self.waitForExpectations(timeout: 10) { error in
            XCTAssert(error == nil, "Test Failed: \(#function) | error: \(error?.localizedDescription ?? "none")")
        }
    }
}
