//
//  Float.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 19/09/2023.
//  Copyright (c) 2023 Jorge Miguel. All rights reserved.
//

import Foundation

extension Float {
    
    public func string(decimals: Int) -> String {
        String(format: "%.\(decimals)f", self)
    }
}
