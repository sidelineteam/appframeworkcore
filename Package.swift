// swift-tools-version:5.3

//-- swift build -Xswiftc "-sdk" -Xswiftc "`xcrun --sdk iphonesimulator --show-sdk-path`" -Xswiftc "-target" -Xswiftc "x86_64-apple-ios13.0-simulator"
//-- swift build -Xswiftc "-sdk" -Xswiftc "`xcrun --sdk iphonesimulator --show-sdk-path`" -Xswiftc "-target" -Xswiftc "x86_64-apple-macos10.14"

import PackageDescription

let package = Package(name: "AppFrameworkCore",
                      
    platforms: [.iOS(.v13), .macOS(.v10_12), .watchOS(.v7)], //, .tvOS(.v10)],

    products: [
        .library(name: "AppFrameworkCore", targets: ["AppFrameworkCore"]),
    ],

    dependencies: [
        //-- Core
        .package(url: "https://github.com/devicekit/DeviceKit", .upToNextMajor(from:"5.5.0")),
        .package(url: "https://github.com/devxoul/Umbrella", from: "0.12.0")
    ],

    targets: [
        //-- targets
        .target(name: "AppFrameworkCore", dependencies: ["DeviceKit", .product(name: "UmbrellaFirebase", package: "Umbrella")], path: "Sources"),

        //-- test target
        .testTarget(name: "AppFrameworkCoreTests", dependencies: ["AppFrameworkCore"], path: "Tests")
    ],

    swiftLanguageVersions: [.v5]
)
