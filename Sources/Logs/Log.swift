//
//  Log.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 05/11/18.
//  Copyright (c) 2018 Jorge Miguel. All rights reserved.
//

import os
import Foundation

public enum LogLevel : Int, Comparable {
    
    case notice  = 4
    case trace   = 3
    case debug   = 2
    case info    = 1
    case warning = 0
    case error   = -1
    case none    = -2
    
    public static func < (lhs: LogLevel, rhs: LogLevel) -> Bool {
        lhs.rawValue < rhs.rawValue
    }
    
    public var description: String {
        switch self {
        case .notice:  return "🪵 ⚪️ NOTICE  |"
        case .trace:   return "🪵 🟣 TRACE   |"
        case .debug:   return "🪵 🌕 DEBUG   |"
        case .info:    return "🪵 🔵 INFO    |"
        case .warning: return "🪵 ⚠️ WARNING |"
        case .error:   return "🪵 🔴 ERROR   |"
        case .none:    return ""
        }
    }
}

public protocol LogProtocol {

    func log(_ level: LogLevel, method: String?, message: Any)
}

public struct Log {
    
    public static var level: LogLevel = .notice

    fileprivate static var shared = Log()

    private var instance: LogProtocol
    
    private var classLevels = [String:LogLevel]()

    //--
    
    init() {
        if #available(iOS 14.0, *) {
            self.instance = OSLogger()
            
        } else {
            self.instance = Logger()
        }
    }

    //-- MARK: configuration

    public static func config(_ instance: LogProtocol) {
        Log.shared.instance = instance
    }
    
    public static func set(_ level: LogLevel, for className: String) {
        Log.shared.classLevels[className] = level
    }
    
    private func level(for methodName: String) -> LogLevel {
//        guard RuntimeConfigs.App.enableLogs else {
//            return .none
//        }
        
        let level = self.classLevels.first { methodName.hasPrefix($0.key) }?.value ?? Log.level
        return (level < Log.level ? level : Log.level)
    }
    
    //-- MARK: log: notice

    public static func notice(_ message: Any) {
        guard Log.level == .notice else { return }
        Log.shared.instance.log(.notice, method: nil, message: message)
    }

    public static func notice(_ method: String, _ message: Any) {
        guard Log.shared.level(for: method) == .notice else { return }
        Log.shared.instance.log(.notice, method: method, message: message)
    }
    
    //-- MARK: log: trace

    public static func trace(_ method: String, _ message: Any? = nil) {
        guard Log.shared.level(for: method) >= .trace else { return }
        Log.shared.instance.log(.trace, method: method, message: message ?? "")
    }
    
    //-- MARK: log: debug

    public static func debug(_ message: Any) {
        guard Log.level >= .debug else { return }
        Log.shared.instance.log(.debug, method: nil, message: message)
    }

    public static func debug(_ method: String, _ message: Any) {
        guard Log.shared.level(for: method) >= .debug else { return }
        Log.shared.instance.log(.debug, method: method, message: message)
    }
    
    //-- MARK: log: info

    public static func info(_ message: Any) {
        guard Log.level >= .info else { return }
        Log.shared.instance.log(.info, method: nil, message: message)
    }

    public static func info(_ method: String, _ message: Any) {
        guard Log.shared.level(for: method) >= .info else { return }
        Log.shared.instance.log(.info, method: method, message: message)
    }
    
    //-- MARK: log: warning

    public static func warning(_ message: Any) {
        guard Log.level >= .warning else { return }
        Log.shared.instance.log(.warning, method: nil, message: message)
    }

    public static func warning(_ method: String, _ message: Any) {
        guard Log.shared.level(for: method) >= .warning else { return }
        Log.shared.instance.log(.warning, method: method, message: message)
    }
    
    //-- MARK: log: error

    public static func error(_ message: Any) {
        guard Log.level >= .error else { return }
        Log.shared.instance.log(.error, method: nil, message: message)
    }

    public static func error(_ method: String, _ message: Any) {
        guard Log.shared.level(for: method) >= .error else { return }
        Log.shared.instance.log(.error, method: method, message: message)
    }

    //-- MARK: log: background
    
    private static let onceToken = "notification.clear.once.key"

    public static func background(_ message: Any, level: LogLevel = .debug) {
        #if DEBUG
        DispatchQueue.once(token: onceToken) {
            LocalNotifications.clearAllNotifications()
        }
        
        LocalNotifications.schedule(title: level.description, message: "\(message)")
        #endif

        Log.shared.instance.log(level, method: nil, message: message)
    }

    public static func background(_ method: String, _ message: Any, level: LogLevel = .debug) {
        #if DEBUG
        DispatchQueue.once(token: onceToken) {
            LocalNotifications.clearAllNotifications()
        }
        
        LocalNotifications.schedule(title: level.description, message: "\(message)")
        #endif

        Log.shared.instance.log(level, method: method, message: message)
    }
    
    //-- MARK: log: timing

    @discardableResult
    public static func timing(_ message: Any, _ startDate: Date? = nil, level: LogLevel = .notice) -> Date {
        let now = Date()
        let diff = (startDate != nil ? " | Diff: \(now.timeIntervalSince(startDate!))" : "")
        Log.shared.instance.log(level, method: nil, message: "\(now.timeIntervalSince1970): \(message)\(diff)")

        return now
    }

    @discardableResult
    public static func timing(_ method: String, _ message: Any, _ startDate: Date? = nil, level: LogLevel = .notice) -> Date {
        let now = Date()
        let diff = (startDate != nil ? " | Diff: \(now.timeIntervalSince(startDate!))" : "")
        Log.shared.instance.log(level, method: method, message: "\(Date().timeIntervalSince1970): \(message)\(diff)")

        return now
    }
}

@available(iOS 14.0, *)
public final class OSLogger : LogProtocol {
    
    private lazy var logger = os.Logger(subsystem: Bundle.main.bundleIdentifier ?? "", category: "main")

    public init() { }

    public func log(_ level: LogLevel, method: String?, message: Any) {
        
        let log: String = if let method = method, method.isEmptyOrWhiteSpaces == false {
            "\(method.padding(toLength: 50, withPad: " ", startingAt: 0)) | \(message)"
            
        } else {
            "\(message)"
        }
        
        switch level {
        case .notice:
            self.logger.notice("\(level.description) \(log)")
        case .trace:
            self.logger.trace("\(level.description) \(log)")
        case .debug:
            self.logger.debug("\(level.description) \(log)")
        case .info:
            self.logger.info("\(level.description) \(log)")
        case .warning:
            self.logger.warning("\(level.description) \(log)")
        case .error:
            self.logger.error("\(level.description) \(log)")
        case .none:
            self.logger.fault("\(level.description) \(log)")
        }
    }
}

public final class Logger : LogProtocol {

    public init() { }

    public func log(_ level: LogLevel, method: String?, message: Any) {
        var log = message
        let methodLen = level == .trace ? 50 : 50
        if let method = method, method.isEmptyOrWhiteSpaces == false {
            log = "\(method.padding(toLength: methodLen, withPad: " ", startingAt: 0)) | \(message)"
        }
        
        print("\(level.description) \(log)")
    }
}
