//
//  Analytics.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 28/06/18.
//  Copyright (c) 2018 Jorge Miguel. All rights reserved.
//

import Umbrella
import DeviceKit
import Foundation
import UmbrellaFirebase

public enum AnalyticsProviders {
    case Firebase
}

//--

open class Analytics {
    
    public static let shared = Analytics()

    private let logger = Umbrella.Analytics<AnalyticsEvents>()
    
    //-- MARK: Setup

    public static func configFirebase() {
        Analytics.shared.logger.register(provider: FirebaseProvider())
    }

    //-- MARK: Events

    public static func eventsProperties(_ properties: [String: Any]) {
        //ARAnalytics.addEventSuperProperties(properties)
    }

    //-- MARK: Success events

    public static func success(_ message: String, properties: [String: Any] = [:], providers: [AnalyticsProviders] = []) {
        Analytics.shared.logger.log(.success(message: message, properties: self.properties(properties)))
    }

    //-- MARK: Checkpoint events

    public static func checkpoint(_ message: String, properties: [String: Any] = [:], providers: [AnalyticsProviders] = []) {
        Analytics.shared.logger.log(.checkpoint(message: message, properties: self.properties(properties)))
    }

    //-- MARK: Error events

    public static func error(_ message: String, properties: [String: Any] = [:], providers: [AnalyticsProviders] = []) {
        Analytics.shared.logger.log(.error(message: message, properties: self.properties(properties)))
    }

    //-- MARK: View Activity

    public static func viewDidAppear(_ name: String, properties: [String: Any] = [:], providers: [AnalyticsProviders] = []) {
        Analytics.shared.logger.log(.viewDidAppear(name: name, properties: self.properties(properties)))
    }

    //-- MARK: Lgin

    public static func login(_ message: String, success: Bool, properties: [String: Any] = [:], providers: [AnalyticsProviders] = []) {
        Analytics.shared.logger.log(.login(message: message, success: success, properties: self.properties(properties)))
    }

    //-- MARK: in-app-purchases (internal)

    public static func inAppPurchase(purchased productId: String, success: Bool, providers: [AnalyticsProviders] = []) {
        Analytics.shared.logger.log(.inAppPurchase(productId: productId, success: success))
    }

    public static func inAppPurchase(restored productId: String, success: Bool, providers: [AnalyticsProviders] = []) {
        Analytics.shared.logger.log(.inAppPurchaseRestore(productId: productId, success: success))
    }

    //-- MARK: helpers

    fileprivate static func properties(_ customProperties: [String: Any] = [:]) -> [String: Any] {
        var props = customProperties

        props["DeviceModel"] = Device.current.description
        props["AppType"] = (RuntimeConfigs.App.isProductionVersion ? "Production" : "Internal")
        props["UserType"] = (RuntimeConfigs.App.isProVersion ? "Free" : "Pro")

        return props
    }
}
