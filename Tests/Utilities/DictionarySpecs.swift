//
//  DictionarySpecs.swift
//  AppFrameworkTests
//
//  Created by Jorge Miguel on 27/04/2020.
//  Copyright © 2020 JMIguel. All rights reserved.
//

import XCTest

@testable import AppFrameworkCore

class DictionarySpecs: XCTestCase {

    func testJSONData() {
        let dict = ["v1": 1,
                    "v2": 2,
                    "v3": "apple",
                    "v4":  "banana",
                    "v5":  [3, 4, "peanuts"]] as [String : Any]
        
        let data = dict.jsonData
        XCTAssert(data?.count ?? 0 > 0)
    }
    
    func testJSON() {
        let dict = ["v1": 1,
                    "v2": 2,
                    "v3": "apple",
                    "v4":  "banana",
                    "v5":  [3, 4, "peanuts"]] as [String : Any]
        
        let json = dict.json
        XCTAssertNotNil(json)
        //XCTAssertEqual(json!, "{\"v3\":\"apple\",\"v2\":2,\"v4\":\"banana\",\"v1\":1,\"v5\":[3,4,\"peanuts\"]}")
    }
    
    func testFirstKey() {
        let dict = ["v1": "1",
                    "v2": "2",
                    "v3": "apple",
                    "v4": "banana"]
        
        XCTAssertEqual(dict.firstKey(where: "2"), "v2")
        XCTAssertNil(dict.firstKey(where: "3"))
    }
    
    func testPlusEqualOperator() {
        var dict1 = ["a": 1, "b": 2]
        let dict2 = ["b": 3, "c": 4]
        
        dict1 += dict2
        
        XCTAssertEqual(dict1, ["a": 1, "b": 3, "c": 4])
    }
    
    func testMapKeys() {
        let dict = ["one": 1, "two": 2, "three": 3, "": 4]
        
        let result = dict.mapKeys({ $0.first }, uniquingKeysWith: { max($0, $1) })
        
        XCTAssertEqual(result["o"], 1)
        XCTAssertEqual(result["t"], 3)
        XCTAssertEqual(result[nil], 4)
    }
    
    func testCompactMapKeys() {
        let dict = ["one": 1, "two": 2, "three": 3, "": 4]
        
        let result = dict.compactMapKeys({ $0.first }, uniquingKeysWith: { max($0, $1) })
        
        XCTAssertEqual(result["o"], 1)
        XCTAssertEqual(result["t"], 3)
        XCTAssertEqual(result.count, 2)
    }
}
