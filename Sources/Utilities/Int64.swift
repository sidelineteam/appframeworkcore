//
//  Int64.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 09/03/2020.
//  Copyright (c) 2020 Jorge Miguel. All rights reserved.
//

import Foundation

extension Int64 {
    
    public var intValue: Int { Int(self) }
    
    public var stringValue: String { self.description }
    
    public var humanReadableFormat: String {
        if (self < 0) {
            return "-\(UInt64(abs(self)).humanReadableFormat)"
            
        } else {
            return UInt64(self).humanReadableFormat
        }
    }
}

extension UInt64 {
    
    public var unsignedIntValue: UInt { UInt(self) }
    
    public var stringValue: String { self.description }
    
    //-- MARK: size format
    
    public var humanReadableFormat: String {
        if (self < 1000) { return "\(self) B" }
        let exp = Int(log2(Double(self)) / log2(1000.0))
        let unit = ["KB", "MB", "GB", "TB", "PB", "EB"][exp - 1]
        let number = Double(self) / pow(1000, Double(exp))
        return String(format: "%.1f %@", number, unit)
    }
}
