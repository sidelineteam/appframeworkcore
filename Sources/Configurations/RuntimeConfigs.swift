//
//  RuntimeConfig.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 21/06/18.
//  Copyright (c) 2018 Jorge Miguel. All rights reserved.
//

import Foundation

extension RuntimeConfigs {

    public struct App {

        //-- MARK: Keys
        
        internal static let kEnableLogs            = RuntimeConfig<Bool>("app.enable.logs", defaultValue: false)

        internal static let kCurrentVersion        = RuntimeConfig<Int>("app.current.version", defaultValue: 100)
        internal static let kCurrentVersionString  = RuntimeConfig<String>("app.current.version.string", defaultValue: "1.0.0")
        internal static let kInstallationVersion   = RuntimeConfig<String>("app.installation.version", defaultValue: "")
        internal static let kInstallationDate      = RuntimeConfig<Date>("app.installation.date", defaultValue: Date())

        internal static let kIsFirstRun            = RuntimeConfig<Bool>("app.is.first.run", defaultValue: true)
        internal static let kIsProVersion          = RuntimeConfig<Bool>("app.is.pro.version", defaultValue: false)

        //-- MARK: Vars
        
        public static var enableLogs: Bool             { get { Configurations.get(kEnableLogs) }           set(newValue) { Configurations.set(kEnableLogs, value: newValue) } }

        public static var currentVersion: Int          { get { Configurations.get(kCurrentVersion) }       set(newValue) { Configurations.set(kCurrentVersion, value: newValue) } }
        public static var currentVersionString: String { get { Configurations.get(kCurrentVersionString) } set(newValue) { Configurations.set(kCurrentVersionString, value: newValue) } }
        public static var installationVersion: String  { get { Configurations.get(kInstallationVersion) }  set(newValue) { Configurations.set(kInstallationVersion, value: newValue) } }
        public static var installationDate: Date       { get { Configurations.get(kInstallationDate) }     set(newValue) { Configurations.set(kInstallationDate, value: newValue) } }

        public static var isFirstRun: Bool             { get { Configurations.get(kIsFirstRun) }           set(newValue) { Configurations.set(kIsFirstRun, value: newValue) } }
        public static var isProVersion: Bool           { get { Configurations.get(kIsProVersion) }         set(newValue) { Configurations.set(kIsProVersion, value: newValue) } }

        public static var isProductionVersion: Bool {
            guard let versionStr = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String,
                let version = Int(versionStr.replacingOccurrences(of: ".", with: "")), version > 0 else {
                    return true
            }

            guard let buildStr = Bundle.main.infoDictionary?["CFBundleVersion"] as? String,
                let build = Int(buildStr.replacingOccurrences(of: ".", with: "")), build > 0 else {
                    return true
            }

            return build >= version
        }
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

public class RuntimeConfig<T: Any> {

    //-- configuration key
    internal let key: String

    //-- type of store (local or shared)
    internal let shared: Bool

    //-- data should be archived (temporary solution)
    internal let codable: Bool

    //-- should encrypt? (encrypted configurations are slower to access)
    internal let encrypted: Bool

    //-- default value for the configuration. Should be set for non optional ones
    internal let defaultValue: T?

    public init(_ key: String, defaultValue: T? = nil, encrypted: Bool = false, shared: Bool = false, codable: Bool = false) {
        self.key = key
        self.shared = shared
        self.codable = codable
        self.encrypted = encrypted
        self.defaultValue = defaultValue
    }
}

public class RuntimeConfigs : UserDefaults {
    
    public override init?(suiteName suitename: String?) {
        super.init(suiteName: suitename)
        self.register(defaults: ["app.enable.logs" : false])
    }
    
    //-- MARK: Subscript

    public subscript<T: Codable>(_ config: RuntimeConfig<T>) -> T {
        get { self.get(config) }
        set(newValue) { self.set(config, value: newValue) }
    }

    public subscript<T: Codable>(_ config: RuntimeConfig<T?>) -> T? {
        get { self.get(config) }
        set(newValue) { self.set(config, value: newValue) }
    }

    //-- MARK: Set

    public func set<T: Codable>(_ config: RuntimeConfig<T>, value: T) {
        let key = (config.encrypted ? config.key.sha256 : config.key)
        Log.notice(methodName(), "key: \(key) | value: \(value)")
        
        if config.encrypted {
            try? self.setValue(Data.archive(value).encrypt(key), forKey: key)

        } else if config.codable {
            try? self.setValue(Data.encode(value), forKey: key)

        } else {
            self.setValue(value, forKey: key)
        }
    }

    public func set<T: Codable>(_ config: RuntimeConfig<T?>, value: T?) {
        let key = (config.encrypted ? config.key.sha256 : config.key)

        guard let value = value else {
            Log.warning(methodName(), "Configuration removal should be done using remove(_ config:) method!")
            self.removeObject(forKey: key)
            return
        }
        
        Log.notice(methodName(), "key: \(key) | value: \(value)")

        if config.encrypted {
            try? self.setValue(Data.archive(value).encrypt(key), forKey: key)

        } else if config.codable {
            try? self.setValue(Data.encode(value), forKey: key)

        } else {
            self.setValue(value, forKey: key)
        }
    }

    //-- MARK: Get

    public func get<T: Codable>(_ config: RuntimeConfig<T>) -> T {
        let key = (config.encrypted ? config.key.sha256 : config.key)

        guard let value = self.value(forKey: key) else {
//            DispatchQueue.once(token: key) {
//                Log.warning(methodName(), "There is no value for the requested configuration: \(config.key). This log will only appear once.")
//            }
            return config.defaultValue!
        }

        if config.encrypted {
            guard let data = value as? Data else {
                return config.defaultValue!
            }

            let archive = data.decrypt(key) as? Data
            return archive?.unarchive() ?? config.defaultValue!
        }

        if config.codable {
            guard let data = value as? Data else {
                return config.defaultValue!
            }

            return try! data.decode() ?? config.defaultValue!
        }

        return value as? T ?? config.defaultValue!
    }

    public func get<T: Codable>(_ config: RuntimeConfig<T?>) -> T? {
        let key = (config.encrypted ? config.key.sha256 : config.key)

        guard let value = self.value(forKey: key) else {
            Log.warning(methodName(), "There is no value for the requested configuration: \(config.key)")
            return nil
        }

        if config.encrypted {
            guard let data = value as? Data else {
                return config.defaultValue as? T
            }

            let archive = data.decrypt(key) as? Data
            return archive?.unarchive() ?? config.defaultValue as? T
        }

        if config.codable {
            guard let data = value as? Data else {
                return config.defaultValue!
            }

            return try? data.decode() ?? config.defaultValue as? T
        }

        return value as? T ?? config.defaultValue as? T
    }

    //-- MARK: Remove

    public func remove<T>(_ config: RuntimeConfig<T>) {
        let key = (config.encrypted ? config.key.sha256 : config.key)
        self.removeObject(forKey: key)
    }
}
