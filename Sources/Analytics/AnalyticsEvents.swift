//
//  AnalyticsEvents.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 20/01/2020.
//  Copyright (c) 2020 Jorge Miguel. All rights reserved.
//

import Umbrella

enum AnalyticsEvents {
    case checkpoint(message: String, properties: [String: Any])
    case error(message: String, properties: [String: Any])
    case inAppPurchase(productId: String, success: Bool)
    case inAppPurchaseRestore(productId: String, success: Bool)
    case login(message: String, success: Bool, properties: [String: Any])
    case success(message: String, properties: [String: Any])
    case viewDidAppear(name: String, properties: [String: Any])
}

extension AnalyticsEvents: EventType {
    
    //-- An event name to be logged
    func name(for provider: ProviderType) -> String? {
        switch self {
            case .checkpoint:           return "Checkpoint"
            case .error:                return "Error"
            case .inAppPurchase:        return "InAppPurchase"
            case .inAppPurchaseRestore: return "InAppPurchaseRestore"
            case .login:                return "Login"
            case .success:              return "Success"
            case .viewDidAppear:        return "ViewDidAppear"
        }
    }
    
    //-- Parameters to be logged
    func parameters(for provider: ProviderType) -> [String: Any]? {
        switch self {
            case let .checkpoint(message, properties):          return ["message": message, "properties": properties]
            case let .error(message, properties):               return ["message": message, "properties": properties]
            case let .inAppPurchase(productId, success):        return ["productId": productId, "success": success]
            case let .inAppPurchaseRestore(productId, success): return ["productId": productId, "success": success]
            case let .login(message, success, properties):      return ["message": message, "success": success, "properties": properties]
            case let .success(message, properties):             return ["message": message, "properties": properties]
            case let .viewDidAppear(name, properties):          return ["name": name, "properties": properties]
        }
    }
}
