//
//  Target.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 18/05/2020.
//  Copyright (c) 2020 Jorge Miguel. All rights reserved.
//

import Foundation

public struct Target {
    
    public static var isTestTarget: Bool = { NSClassFromString("XCTest") != nil }()
    
    public static var isDebuggingNetwork: Bool = { CommandLine.arguments.contains("-network-debug") }()
    
    public static var isiOSAppOnMac: Bool {
      if #available(iOS 14.0, *) {
        return ProcessInfo.processInfo.isiOSAppOnMac
      }
        
      return false
    }
    
    public static var isRunningOnCanvas: Bool {
        ProcessInfo.processInfo.environment["XCODE_RUNNING_FOR_PREVIEWS"] == "1"
    }
}
