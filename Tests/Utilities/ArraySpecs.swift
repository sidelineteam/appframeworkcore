//
//  ArraySpecs.swift
//  AppFrameworkTests
//
//  Created by Jorge Miguel on 12/03/2020.
//  Copyright © 2020 JMiguel. All rights reserved.
//

import XCTest

class ArraySpecs: XCTestCase {

    func testChunked() {
        let array = [1, 2, 3, 4, 5, 6, 7]
        let chunkedArray = array.chunked(into: 3)
        XCTAssertEqual(chunkedArray, [[1, 2, 3], [4, 5, 6], [7]])
        
        let emptyArray: [Int] = []
        XCTAssertEqual(emptyArray.chunked(into: 2), [])
        
        let smallArray = [1, 2]
        XCTAssertEqual(smallArray.chunked(into: 3), [[1, 2]])
    }

    func testJSONData() {
        let array = [1, 2, "apple", "banana", [3, 4, "peanuts"]] as [Any]
        let data = array.jsonData
        XCTAssert(data?.count ?? 0 > 0)
    }
    
    func testJSON() {
        let array = [1, 2, "apple", "banana", [3, 4, "peanuts"]] as [Any]
        let json = array.json
        XCTAssertEqual(json, "[1,2,\"apple\",\"banana\",[3,4,\"peanuts\"]]")
    }
    
    func testRemoveDuplicates() {
        let array = [1, 2, 3, 5, 4, 2, 1]
        let reduced = array.removeDuplicates
        XCTAssertEqual(reduced, [1, 2, 3, 5, 4])
    }
    
    func testDifferenecs() {
        let array1 = ["apple", "banana", "pear"]
        let array2 = ["apple", "nuts", "pear"]
        XCTAssertEqual(array1.differences(from: array2), ["banana", "nuts"])
    }
    
    func testRemoveObject() {
        let item = "wow"
        var array = ["12", "34", item]
        array.remove(object: item)
        XCTAssertEqual(array, ["12", "34"])
    }
    
    func testReplaceObject() {
        let obj1 = TestObject(id: 1, value: "original")
        let obj2 = TestObject(id: 2, value: "new")
        let obj3 = TestObject(id: 1, value: "replacement")
        let obj4 = TestObject(id: 3, value: "another original")
        
        var arr = [obj1, obj4]
        XCTAssertEqual(arr.count, 2)
        
        var pos = arr.replace(obj2)
        XCTAssertEqual(arr.count, 2)
        XCTAssertNil(pos)
        
        
        pos = arr.replace(obj3)
        XCTAssertEqual(arr.count, 2)
        XCTAssertEqual(arr[pos!].value, obj3.value)
    }
    
    func testLinearDiffEqual() {
        var arr1 = ["0", "1", "2", "3", "4"]
        let arr2 = ["1", "2", "3"]
        let final = ["0", "1", "2", "3", "4"]
        
        arr1.update(with: arr2)
        XCTAssertEqual(final, arr1)
    }
    
    func testLinearDiffAdd() {
        var arr1 = ["0", "1", "3", "4"]
        let arr2 = ["1", "2", "3"]
        let final = ["0", "1", "2", "3", "4"]
        
        arr1.update(with: arr2)
        XCTAssertEqual(final, arr1)
    }
    
    func testLinearDiffRemove() {
        var arr1 = ["0", "1", "2", "4", "5"]
        let arr2 = ["1", "3", "4"]
        let final = ["0", "1", "3", "4", "5"]
        
        arr1.update(with: arr2)
        XCTAssertEqual(final, arr1)
    }
}

fileprivate struct TestObject: Identifiable {
    
    var id: Int
    var value: String
}
