//
//  NSLockSpecs.swift
//  AppFrameworkCore
//
//  Created by Jorge Miguel on 18/02/2025.
//

import XCTest
import AppFrameworkCore

class NSLockSpecs: XCTestCase {
    
    func testExecute_WithSuccessfulBlock_ReturnsExpectedValue() {
        // Given
        let expectedValue = 42
        
        // When
        let result = NSLock.execute {
            return expectedValue
        }
        
        // Then
        XCTAssertEqual(result, expectedValue)
    }
    
    func testExecute_WithThrowingBlock_PropagatesError() {
        // Given
        enum TestError: Error {
            case someError
        }
        
        // Then
        XCTAssertThrowsError(try NSLock.execute {
            throw TestError.someError
        }) { error in
            XCTAssertTrue(error is TestError)
        }
    }
    
    func testExecute_WithConcurrentAccess_PreservesThreadSafety() {
        // Given
        let iterations = 250
        var sharedCounter = 0
        let expectation = XCTestExpectation(description: "Concurrent operations completed")
        
        // When
        DispatchQueue.concurrentPerform(iterations: iterations) { _ in
            NSLock.execute {
                sharedCounter += 1
                if sharedCounter == iterations {
                    expectation.fulfill()
                }
            }
        }
        
        // Then
        wait(for: [expectation], timeout: 5.0)
        XCTAssertEqual(sharedCounter, iterations)
    }
}
