//
//  URLSpecs.swift
//  AppFrameworkTests
//
//  Created by Jorge Miguel on 26/10/2023.
//  Copyright © 2023 JMiguel. All rights reserved.
//

import XCTest

class URLSpecs: XCTestCase {
    
    func testRelativePath() {
        let baseURL = URL(fileURLWithPath: "/Users/test/Documents")
        let fileURL = URL(fileURLWithPath: "/Users/test/Documents/folder/file.txt")
        let siblingURL = URL(fileURLWithPath: "/Users/test/Documents/other/sibling.txt")
        let nonFileURL = URL(string: "https://example.com")!
        
        // Test relative path from base to file
        XCTAssertEqual(fileURL.relativePath(from: baseURL), "folder/file.txt")
        
        // Test relative path between siblings
        XCTAssertEqual(siblingURL.relativePath(from: fileURL), "../other/sibling.txt")
        
        // Test with non-file URLs
        XCTAssertNil(nonFileURL.relativePath(from: baseURL))
        XCTAssertNil(fileURL.relativePath(from: nonFileURL))
    }
    
    func testMoveFile() {
        // Create temporary directory for testing
        let tempDir = FileManager.default.temporaryDirectory
        
        // Create a test file
        let sourceURL = tempDir.appendingPathComponent("source.txt")
        try? "test content".write(to: sourceURL, atomically: true, encoding: .utf8)
        
        // Test moving file with new name in same directory
        let movedURL1 = sourceURL.moveFile(as: "moved.txt")
        XCTAssertFalse(FileManager.default.fileExists(atPath: sourceURL.path))
        XCTAssertTrue(FileManager.default.fileExists(atPath: movedURL1.path))
        XCTAssertEqual(movedURL1.lastPathComponent, "moved.txt")
        
        // Test moving file to different directory
        let newDir = tempDir.appendingPathComponent("newdir")
        try? FileManager.default.createDirectory(at: newDir, withIntermediateDirectories: true)
        let movedURL2 = movedURL1.moveFile(to: newDir, as: "moved2.txt")
        XCTAssertFalse(FileManager.default.fileExists(atPath: movedURL1.path))
        XCTAssertTrue(FileManager.default.fileExists(atPath: movedURL2.path))
        XCTAssertEqual(movedURL2.lastPathComponent, "moved2.txt")
        
        // Clean up
        try? FileManager.default.removeItem(at: newDir)
    }
    
    func testQueryItem() {
        let url = "http://url.com?var1=1&var2=2&var3=3".url
        let url1 = "http://url.com?var=".url
        
        XCTAssertEqual(url?.queryItem("var1"), "1")
        XCTAssertEqual(url?.queryItem("var2"), "2")
        XCTAssertEqual(url?.queryItem("var3"), "3")
        XCTAssertEqual(url?.queryItem("var4"), nil)
        XCTAssertEqual(url1?.queryItem("var"), "")
    }

    func testRemovingQueryItem() {
        let url = "http://url.com?var1=1&var2=2&var3=3".url
        let url1 = "http://url.com?var2=2&var3=3".url
        let url2 = "http://url.com?var1=1&var3=3".url
        let url3 = "http://url.com?var1=1&var2=2".url
        
        XCTAssertEqual(url?.removingQueryItem("var1"), url1)
        XCTAssertEqual(url?.removingQueryItem("var2"), url2)
        XCTAssertEqual(url?.removingQueryItem("var3"), url3)
        XCTAssertEqual(url?.removingQueryItem("var4"), url)
    }
}
