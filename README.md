# AppFrameworkV2
AppFrameworkV2 is a framework for those who want to start coding their ideas as soon as possible, without having to care about the repetitive code that is required when creating a new project. It is a Swift, more modern version of the AppFramework, written in ObjC.

## How to Get Started

Create the app project including AppFrameworkV2 using `Swift Package Manager`.

Since this framework has a lot of features, sometimes including only a subset might be handy. With that in mind, AppFrameworkV2 is devided in feature targets, each one providing smaller sets of features.

## Installation

### Swift Package Manager

`Swift Package Manager` is the official package manager from Apple. In order to use 'AppFrameworkV2' through it, use the provided tools on XCode. The packages available are:

```swift
AppFrameworkCore
AppFrameworkIAP                 #-- includes AppFrameworkCore
AppFrameworkMocker              #-- includes AppFrameworkCore
AppFrameworkNetwork             #-- includes AppFrameworkCore
AppFrameworkDataStore           #-- includes AppFrameworkCore
```

### Tests

AppFrameworkV2 tests are available on the SPM package.

## Usage

```Swift
// swift-tools-version:5.3

//-- swift build -Xswiftc "-sdk" -Xswiftc "`xcrun --sdk iphonesimulator --show-sdk-path`" -Xswiftc "-target" -Xswiftc "x86_64-apple-ios13.0-simulator"

import PackageDescription

let package = Package(name: "MyApp",
    platforms: [.iOS(.v11), .macOS(.v10_12)],

    products: [
        .library(name: "MyApp", targets: ["MyApp"]),
        .library(name: "MyAppStubs", targets: ["MyAppStubs"])
    ],

    dependencies: [
        //-- AppFramework
        .package(name: "AppFrameworkDataStore", path: "../../appframeworkv2/datastore"),
        .package(name: "AppFrameworkNetwork", path: "../../appframeworkv2/network"),
        .package(name: "AppFrameworkMocker", path: "../../appframeworkv2/mocker"),
        .package(name: "AppFrameworkCore", path: "../../appframeworkv2/core"),
        .package(name: "AppFrameworkIAP", path: "../../appframeworkv2/iap"),
    ],

    targets: [
        .target(name: "MyApp", dependencies: ["AppFrameworkDataStore", "AppFrameworkNetwork"], path: "Sources"),
        .target(name: "MyAppStubs", dependencies: ["MyApp", "AppFrameworkMocker"], path: "Stubs"),

        .testTarget(name: "MyAppTests", dependencies: ["MyAppStubs"], path: "Tests")
    ],

    swiftLanguageVersions: [.v5]
)
```

### AFCore

#### Architecture

* `Configurations`
* `Utils`
* `LocalNotifications`
* `Logs`
* `Analytics`
* `AppUpgrade`

#### Configurations

Configurations allows you to manage the app configurations required for your app in an ubiquitous and seamless way, across static, remote and runtime configurations.

##### Setup

Configurations requires the setup of the configurations being added to the app.

###### Configure - Static configurations

```Swift
extension StaticConfigs {

    struct Analytics {

        struct Provider {

            //-- MARK: Keys
            private static let kCrahslyticsEnabled  = StaticConfig<Bool>("appframework.analytics.provider.crashlytics", defaultValue: false)
            private static let kFirebaseEnabled     = StaticConfig<Bool>("appframework.analytics.provider.firebase", defaultValue: false)

            //-- MARK: Vars
            internal static var crahslyticsEnabled: Bool  { Configurations.get(kCrahslyticsEnabled) }
            internal static var firebaseEnabled: Bool     { Configurations.get(kFirebaseEnabled) }
        }
    }
}
```

In order to add new static configurations, extend the class `StaticConfigs` and declare the new configuration as seen above. Organizing the configurations by using structs is optional.

In order to define the value for the configurations, add them to the `static.plist`.

For encrypted configurations, a tool is provided to generate the encrypted values. The values will be presented in the logs of the app (in dev mode) and should be copied to the `static.plist`.

```Swift
public init(_ key: String, defaultValue: ValueType, encrypted: Bool = false)
```

##### Methods

```Swift
public init(_ key: String, defaultValue: ValueType, encrypted: Bool = false)
```

Instantiates a static configuration, specifying if it is encrypted and its default value.

###### Configure - Runtime configurations

```Swift
extension RuntimeConfigs {

    public struct App {

        //-- MARK: Keys
        internal static let kInstallationVersion   = RuntimeConfig<String>("app.installation.version", defaultValue: "")
        internal static let kInstallationDate      = RuntimeConfig<Date>("app.installation.date", defaultValue: Date())

        //-- MARK: Vars
        public static var installationVersion: String  { get { Configurations.get(kInstallationVersion) }  set(newValue) { Configurations.set(kInstallationVersion, value: newValue) } }
        public static var installationDate: Date       { get { Configurations.get(kInstallationDate) }     set(newValue) { Configurations.set(kInstallationDate, value: newValue) } }
	}
}
```

In order to add new runtime configurations, extend the class `RuntimeConfigs` and declare the new configuration as seen above. Organizing the configurations by using structs is optional.

##### Methods

```Swift
public init(_ key: String, defaultValue: T? = nil, encrypted: Bool = false, shared: Bool = false, codable: Bool = false)
```

Instantiates a runtime configuration, specifying if it is encrypted, shared and / or codable, as well as its default value.

###### Configure - Remote configurations

```Swift
extension RemoteConfigs {

    struct Category {
        static let configName = "ios.config.name"
    }
}
```

In order to add new remote configurations, extend the class `RemoteConfigs` and declare the new configuration as seen above. organizing configurations by using of structs is optional.

In order to define the value for the configuration, add them to the `remote.plist`.

##### Methods

```Swift
public static func config(staticPlistPath path: String? = nil, staticEncryptionKey key: String? = nil)
```
Defines a custom path to the `static.plist` file. It also can define the encryption key for the static configurations. Should be used as soon as possible (when required).

```Swift
public func setDefaults(_ values: [String: Any], shared: Bool = false)
```
Defines the default values for the app's custom runtime configurations. It should be called as soon as possible (AppDelegate init for example).
Note: These values won't persist across sessions, therefore they should be static values and not values like a Date (for example a configuration for the 1st launch date...)

```Swift
public static func staticConfigurationsHelper(_ configurations: [String: String])
```
Logs the encrypted values for the given static configurations. To be used on only on dev.

```Swift
public static func get<T>(_ key: String) -> T?
```
Returns the value available for the remote configuration.

```Swift
public static func get<T>(_ config: StaticConfig<T>) -> T
```
Returns the value available for the static configuration.

```Swift
//-- non optional
public static func get<T: Codable>(_ config: RuntimeConfig<T>) -> T

//-- optional
public static func get<T: Codable>(_ config: RuntimeConfig<T?>) -> T?
```
Returns the value available for the runtime configuration.

```Swift
//-- non optional
public static func set<T: Codable>(_ config: RuntimeConfig<T>, value: T)

//-- optional
public static func set<T: Codable>(_ config: RuntimeConfig<T?>, value: T?)
```
Sets the value for the runtime configuration.

```Swift
public static func remove<T>(_ config: RuntimeConfig<T>)
```
Resets the value of the runtime configuration.

##### How to use

```Swift
//-- configures the encryption key for the static configurations
//-- better add this on app delegate's didFinishLaunchingWithOptions
Configurations.config(staticEncryptionKey: "your_amazing_encryption_key".md5())
```
```Swift
//-- This will log the values for the keys that should be added encrypted on the static.plist
//-- This will only log on test builds, but is probably a good idea to remove the original values before sending the store
Configurations.staticConfigurationsHelper([StaticConfigs.InAppPurchases.sharedSecret.key : "original_shared_secret_key",
                                           StaticConfigs.Feedback.key.key: "original_send_grid_key"])
```
```Swift
//-- read value (encrypted is for static and runtime configurations only)
let version = Configurations.get(StaticConfigs.App.currentVersion, encrypted = true)
let version = Configurations.get(RuntimeConfigs.App.currentVersion, encrypted = true)
```
```Swift
//-- update value (runtime only)
Configurations.set(RuntimeConfigs.App.currentVersion, value: currentVersion, encrypted = true)
```
```Swift
//-- check if value is set (runtime only)
Configurations.isset(RuntimeConfigs.App.currentVersion, encrypted = true)
```
```Swift
//-- reset value (runtime only)
Configurations.reset(RuntimeConfigs.App.currentVersion, encrypted = true)
```

#### Utils

Utils is a set of extensions to the standard classes.

##### Methods

`Array`

```Swift
public var json: String?
```

Returns the content of the Array as a JSON string.`

```Swift
public var jsonData: Data?
```

Returns the content of the Array as a Data object representing the Array as JSON.

```Swift
public var removeDuplicates: [Element] {
```

Removes all duplicated values on the Array, mantaining the order.

```Swift
 @discardableResult public mutating func remove(object: Element) -> Bool
```

Removes a given object from the Array.

`ClassInfo`

This utility provides the name of the current class and method, if used through a subclass of NSObject or `ClassInfo`. It can also be used statically, by receiving the object as parameter.

```Swift
public var className -> String

public class func className(_ obj: Any) -> String
```
Returns the class name.

```Swift
public func methodName(_ f: String = #function) -> String

public class func methodName(_ obj: Any, f: String = #function) -> String
```
Returns the method name.

`Color`

```Swift
public var uiColor: UIColor
```

Provides a UIColor object representing the same color of the Color object.

```Swift
public init(hex: String)
```

Provides a Color object from the hexadecimal code of the color.

`Data`

Data extension with support for hashing, encryption and archiving of data.

```Swift
public var utf8String: String?
```

Returns the String representation of the data, encoded with utf8 format.

```Swift
public var sha256: [UInt8]
```

Returns the sha256 hash of the data.

```Swift
public static func random(_ size: Int) -> Data
```

Returns a Data object filled with random data.

```Swift
public static func archive<T>(_ value: T) throws -> Data

public func unarchive<T>() -> T?
```
Methods to archive / unarchive data.

```Swift
public static func encode<T: Codable>(_ value: T) throws -> Data

public func decode<T: Codable>() throws -> T?
```
Methods to encode / decode Codable objects.

```Swift
public func encrypt(_ salt: String) -> Data

public func decrypt(_ salt: String) -> Any?
```
Methods to encrypt / decrypt data.

```Swift
public func subdata(in range: ClosedRange<Index>) -> Data?
```
Subdata alternative implementation.

`Date`

Date extension with support for date formatting.

```Swift
public var isoDate: String
```

Returns the String representation of the date with the iso format.

```Swift
public var dateTimeFormat: String
```

Returns the String representation of the date with the format `dd-MM-yyyy HH:mm:ss`.

`Dictionary`

```Swift
public static func +=(lhs: inout [Key: Value], rhs: [Key: Value])
```
Appends a Dictionary into another.

```Swift
public var json: String?
```

Returns the content of the Dictionary as a JSON string.`

```Swift
public var jsonData: Data?
```

Returns the content of the Dictionary as a Data object representing the Dictionary as JSON.

`DispatchQueue`

DispatchQueue extension with support for dispatch once and helper methods.

```Swift
public static func syncOnMain<T>(_ work: () throws -> T) rethrows -> T
```

Executes the block synchornizely on the main thread.

```Swift
public class func once(file: String = #file, function: String = #function, line: Int = #line, block: DispatchQueueExecutionBlock)

@discardableResult
public class func once(token: String, block: DispatchQueueExecutionBlock) -> Bool
```

Provides an handler that will guarantee that the embeded code is only executed once. Returns true if the handler is to be executed.

```Swift
public class func resetOnce(token: String)
```

Resets the lock for the once executer associated to the given token.

`Int64` (also for `UInt64`)

Int64 extension with support for formats.

```Swift
public var intValue: Int
```

Returns an `Int` object with the value based on the current value.

```Swift
public var StringValue: String
```

Returns a `String` object based on the current value.

```Swift
public var humanReadableFormat: String
```

Returns the String representation of the amount of bytes represented by the Int64, in a readable format.

`Mirror`

```Swift
public static func reflectProperties<T>(of target: Any, using handler: PropertyHandler<T>)
```

Iterates all the properties of the class of the defined type. Stops if the handler returns `true`

`NSObject`

```Swift
public func synchronized<T>(_ lockObj: AnyObject, closure: () throws -> T) rethrows -> T
```

Synchronizes the access to the `lockObj` object, returning the result of the handling.

`PHAsset`

```Swift
public var fileName: String?
public var fileSize: Int64?
```
Returns useful asset's information.

```Swift
public func fileUrl(_ handler : @escaping AssetFileURLHandler)
```
Returns the URL of the asset's content.

```Swift
public func thumbnail(width: Int, height: Int, synchronized: Bool = false, handler : @escaping AssetImageHandler)
```
Returns the asset's thumbnail that fit the given dimensions. The completion handler will be executed synchronosly if `synchronized` is set to true.

```Swift
public func thumbnail(synchronized: Bool = false, handler : @escaping AssetImageHandler)
```
Returns the asset's thumbnail. The completion handler will be executed synchronosly if `synchronized` is set to true.

```Swift
public func fullResolution(synchronized: Bool = false, handler : @escaping AssetImageHandler)
```

Returns the asset's full resolution image. The completion handler will be executed synchronosly if `synchronized` is set to true.

```Swift
public func data(synchronized: Bool = false, handler : @escaping AssetImageDataHandler)
```

Returns the asset's data. The completion handler will be executed synchronosly if `synchronized` is set to true.

`String`

```Swift
public var url: URL?
```

Returns a URL based on the  String.

```Swift
public var image: UIImage?
```

Returns a UIImage based on the String.

```Swift
public var urlQueryEncoded: String? { self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
```

Returns the url encoded version of the String that can be used as query string on a url.

```Swift
public var isEmptyOrWhiteSpaces: Bool
```

Returns true if the string is empty or composed of white spaces and / or new lines.

```Swift
public var color: Color
```

Returns a `Color` based on the String. It will return `.white` if the string is not a valid hexadecimal code.

```Swift
public func font(_ size: CGFloat) -> Font
```

Returns a `Font` with the name based on current String and the given size.

```Swift
public var localized: String
```

Returns the localized version of the string.

```Swift
public var localizedUppercase: String
```

Returns the localized version of the string uppercase.

```Swift
public var localizedCapitalized: String
```

Returns the localized version of the string capitalized.

```Swift
public func localized(_ elements: [String:String]? = nil, count: Int? = nil) -> String
```

Returns the localized version of the string, replacing all the ${`key`} occurrences by each corresponding value.
Also will consider cardinality if `count` != nil. In this case the localization keys should be formatted like `key_1` and `key_n`.

```Swift
public var sha256: String
```

Returns the sha256 hash of the String.

```Swift
public func encrypt(_ salt: String) -> String?
public func decrypt(_ salt: String) -> Any?
public func decryptString(_ salt: String) -> String?
```
Methods to encrypt / decrypt the String.

```Swift
public var isoDate: Date?
public func date(_ format: String) -> Date?
```

Return a Date based on the String.

```Swift
public var isValidEmail: Bool
```

Validates if the String is a valid email.

```Swift
public var isValidUrl: Bool
```

Validates if the String is a valid URL.

```Swift
public func match(_ regex: String) -> [[String]]
```

Returns the substrings matching the regex filter. The first element of the inner `Array` is the full match; the remaining elements are the captured substrings.

`Target`

Provides information on the current Target.


```Swift
public static var isTestTarget: Bool
```

Returns `true` if the current Target is a test target.


```Swift
public static var isDebuggingNetwork: Bool
```

Returns `true` if the Scheme is configured to debug network calls. This is achieved by passing the launch argument `-debug-network`.

`UIColor`

UIColor extension with convenience initializers and some extra commonly used colors.

```Swift
public class var darkerGray: UIColor

public class var lighterGray: UIColor

public class var dirtyWhite: UIColor

//-- dynamic colors
public class var appBackground: UIColor

public class var appHighlight: UIColor

public class var appSelected: UIColor

public class var appTitle: UIColor
```

These colors are based on the color set as the default navigation bar's color for the app. (through the appearance of the `UINavigationBar`)


```Swift
public convenience init(rgb: String)
```

Initializer that receives the hex code of the color.


```Swift
public func lighterColor(delta: CGFloat = 0.2) -> UIColor
```

Returns a lighter version of the current color.

`UIImage`

```Swift
public static func load(_ named: String, resourcesBundle bundleName: String = "AppFrameworkV2") -> UIImage?
```

Loads an image from a bundle with the given name.`

```Swift
public var ratio: CGFloat
```

The original ratio of the image.

```Swift
public var effectNoir: UIImage
```
Returns a copy of the image with a noir effect applied

```Swift
public func resize(_ size: CGSize, isOpaque: Bool = false) -> UIImage
```

Returns a resized copy of the image.

```Swift
public static func resize(from path: URL, size: CGSize) -> UIImage
```

Returns a resized copy of the image at `path`.

```Swift
public var normalizedOrientation: UIImage?
```

Returns a copy of the image rotateted to the expected orientation.

`UIImageView`

```Swift
public func blurEffect(style: UIBlurEffectStyle)
```
Applies a blur effect to an image by using a UIBlurEffectStyle.

```Swift
public func blurEffect(radius: Int)
```
Applies a blur effect to an image by using a radius constant.

`UILabel`

```Swift
public var contentInsets: UIEdgeInsets?
```
Applies padding space around the text on the label.

`UIView`

```Swift
public var safeArea: ConstraintBasicAttributesDSL
```
Provides an easy way to use SafeAreas constraints.

```Swift
public func applyGradient(colours: [UIColor], gradientOrientation orientation: GradientOrientation)
```
Applies a gradient to the background of a view.

`URL`

```Swift
public func relativePath(from base: URL) -> String?
```

Returns the path relative to the given base URL.

`View`

```Swift
public func eraseToAnyView() -> AnyView
```

Erases the `View` type to `AnyView`.

```Swift
@ViewBuilder
public func `if`<Transform: View>(_ condition: Bool, transform: IfHandler) -> some View
```

Allows the conditional usage of modifiers on a `View`.

#### XMLContentParser

XMLContentParser parses a XML document, searching for the tags specified on `elements` and their contents. When finished returns a `Dictionary` with the found pairs.

```Swift
public init(with parser: XMLParser, find elements: [String], handler: @escaping XMLContentParserHandler)
```

#### LocalNotifications

`LocalNotifications` manges the authorization, creation and delivery of local notifications.

##### Methods & Vars

```Swift
public static func requestAuthorization()
```

Requests authorization to deliver notifications to the user.

```Swift
public static func schedule(title: String, message: String = "")
```

Schedules a notification to be delivered in the next second.

```Swift
public static func clearAllNotifications()
```

Clears all the scheduled and delivered notifications.

#### Logs

Logs provides a plugin interface to create custom log engines. If no customisation is applied, the default print() behaviour is applied.

##### Plugin Protocol

```Swift
public protocol LogProtocol {

    func log(_ level: LogLevel, method: String?, message: Any)
}
```

* `level` - The level of the log.
* `method` - The name of the method where the log originated from.
* `message` - The log message.

##### Methods & Vars

```Swift
public static var level: LogLevel = .verbose
```

Defines the maximum log level presented on console.

```Swift
public static func config(_ instance: LogProtocol)
```

Configures the plugin that will be handling the logs.

```Swift
public static func set(_ level: LogLevel, for className: String)
```

Configures the log level to be assigned to a class, by name.


```Swift
public static func verbose(_ message: Any)
public static func verbose(_ method: String, _ message: Any)

public static func trace(_ method: String, _ message: Any? = nil)

public static func debug(_ message: Any)
public static func debug(_ method: String, _ message: Any)

public static func info(_ message: Any)
public static func info(_ method: String, _ message: Any)

public static func warning(_ message: Any)
public static func warning(_ method: String, _ message: Any)

public static func error(_ message: Any)
public static func error(_ method: String, _ message: Any)
```
These methods will create standard logs, each one for a specific log level.

```Swift
public static func timing(_ message: Any, _ startDate: Date? = nil, level: LogLevel = .verbose) -> Date
public static func timing(_ method: String, _ message: Any, _ startDate: Date? = nil, level: LogLevel = .verbose) -> Date
```
These methods will create a log with time span information on the configured plugin. This is quite useful when testing actions' durations.

```Swift
public static func background(_ message: Any, level: LogLevel = .debug)
public static func background(_ method: String, _ message: Any, level: LogLevel = .debug)
```
These methods will create a log on the configured plugin and dispatch a local notification. This is quite useful when testing apps in background. The local notifications will only be dispatched in DEBUG mode.

#### Analytics

Analytics provides the support for several analytics platforms, all under the same roof. You can choose to log events to a single platform, a custom set of platforms (by event), or to all the platforms configured in a single call.

##### Setup

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>appframework</key>
    <dict>
        <key>analytics</key>
        <dict>
            <key>providers</key>
            <dict>
                <key>firebase</key>
                <false/>
            </dict>
        </dict>
    </dict>
</dict>
</plist>
```

On `statics.plist` (under `appframework.analytics.providers`):

* Enable the desired platform(s).
* Configure the platforms, as required by each of them.

##### Methods

```Swift
public static func configFirebase()
```
Configures the Firebase platform

```Swift
public static func eventsProperties(_ properties: [String: Any])
```
sets properties that will be forwarded as part of every event

```Swift
public static func success(_ message: String, properties: [String: Any] = [:], providers: [AnalyticsProviders] = [])
```
Logs a success event on the defined platforms, or on all of them if none is defined

```Swift
public static func checkpoint(_ message: String, properties: [String: Any] = [:], providers: [AnalyticsProviders] = [])
```
Logs a checkpoint event on the defined platforms, or on all of them if none is defined`

```Swift
public static func error(_ message: String, properties: [String: Any] = [:], providers: [AnalyticsProviders] = [])
```
Logs an error event on the defined platforms, or on all of them if none is defined`

```Swift
public static func viewDidAppear(_ name: String, properties: [String: Any] = [:], providers: [AnalyticsProviders] = [])
```
Logs a viewDidAppear event on the defined platforms, or on all of them if none is defined`

```Swift
public static func login(_ message: String, success: Bool, properties: [String: Any] = [:], providers: [AnalyticsProviders] = [])
```
Logs a Login event on the defined platforms, or on all of them if none is defined`

##### How to use

```Swift
func application(_ application: UIApplication,  launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    //-- setup analytics platforms
    Analytics.configFirebase()
}
```
During the app launch, register the analytics platforms that you want to use.

```Swift
Analytics.success("WE DID IT!!!!", parameters: ["iteration": 1000], providers: [.Firebase])
```
Here we are logging a success event on Firebase. Both `parameters` and `providers` could be omitted, in case of not being required.

#### AppUpgrade

AppUpgrade allows you to manage app upgrade migrations without effort.

##### Methods

```Swift
public static func validateAppVersionUpgrade(_ handler: AppUpgradeCallback)
```

Validates the current app version against the app version on the last launch.

##### How to use

```Swift
extension AppUpgrade {

    private static let kUpdateVersion_1_0_0 = 100
    private static let kUpdateVersion_1_x_x = 1xx
    ...

    public static func validate() {
        self.validateAppVersionUpgrade { (upgraded, prevVersion) in
            if upgraded {
                if prevVersion < kUpdateVersion_1_0_0 {
                    //-- do stuff...
                }
                if prevVersion < kUpdateVersion_1_x_x {
                    //-- do stuff...
                }
                //--- ...
            }
        }
    }

}
```

To use AppUpgrade, simply extend the AppUpgrade class and create the method `validate()` as seen above. Then call the `validate()` method on your AppDelegate during launch.

AppUpgrade checks `CFBundleShortVersionString` to identify the current version number. For this and other reasons, I usually match this value to the version number when releasing to the store.

---

## Dependencies

On this version of AppFramework I choose to use 3rd party libs as much as possible... and justifiable. The libs listed below are the core of some of the AppFrameworkV2's features.

```
FirebaseRemoteConfig #-- Remote configurations  (Core)

Umbrella             #-- Analytics              (Core)

Umbrella/Firebase    #-- Analytics              (Core)

SwiftyStoreKit       #-- InAppPurchases         (IAP)

Realm                #-- Storage                (DataStore)
```

Adding to these, some other libs where also used on AppFrameworkV2 to provide internal features to the AppFrameworkV2, hence not having been wrapped by AppFrameworkV2.

```
DeviceKit         #-- Device info                                (Core)

ReachabilitySwift #-- Network Reachability                       (Network)
```

---

## Author

Jorge Miguel

## License

AppFrameworkV2 is available under the MIT license. See the LICENSE file for more info.
