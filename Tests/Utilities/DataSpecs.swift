//
//  DataSpecs.swift
//  AppFrameworkTests
//
//  Created by Jorge Miguel on 27/04/2020.
//  Copyright © 2020 JMIguel. All rights reserved.
//
import XCTest

@testable import AppFrameworkCore

struct CodableStruct: Codable, Equatable {
    var field1: String = "value1"
    var field2: Int = 2
    var field3: [String] = ["v3", "v4"]
}

class DataSpecs: XCTestCase {
    
    //-- MARK: convertion
    
    func testUTF8String() {
        let data = "text".data(using: .utf8)!
        XCTAssertEqual(data.utf8String, "text")
    }
    
    //-- MARK: hashing
    
    func testMD5() {
        let data = "test".data(using: .utf8)!
        let hash = data.md5
        XCTAssertEqual(hash.count, 16) // MD5 is always 16 bytes
    }

    func testSHA256() {
        let data = "test".data(using: .utf8)
        let hash: [UInt8] = [159, 134, 208, 129, 136, 76, 125, 101, 154, 47, 234, 160, 197, 90, 208, 21, 163, 191, 79, 27, 43, 11, 130, 44, 209, 93, 108, 21, 176, 240, 10, 8]
        XCTAssertEqual(data?.sha256, hash)
    }
    
    //-- MARK: random
    
    func testRandom() {
        let size = 1024
        let data = Data.random(size)
        XCTAssertEqual(data.count, size)
    }
    
    //-- MARK: encryption
    
    func testEncryptDecrypt() {
        let salt = "salt"
        let data = "text".data(using: .utf8)!
        let encrypted = data.encrypt(salt)
        let decrypted = encrypted?.decrypt(salt)
        XCTAssertEqual(data, decrypted as? Data)
    }
    
    //-- MARK: archive (Codable)
    
    func testEncode() {
        let encodable = CodableStruct()
        let encoded = try? Data.encode(encodable)
        XCTAssertNotNil(encoded)
    }
    
    func testDecode() {
        let original = CodableStruct()
        let encoded = try? Data.encode(original)
        XCTAssertNotNil(encoded)
        
        let decoded: CodableStruct? = try? encoded?.decode()
        XCTAssertEqual(original, decoded)
    }
    
    func testDecodeWithType() {
        let original = CodableStruct()
        let encoded = try? Data.encode(original)
        XCTAssertNotNil(encoded)
        
        let decoded = try? encoded?.decode(CodableStruct.self)
        XCTAssertEqual(original, decoded)
    }
    
    //-- MARK: archive
    
    func testArchive() {
        let object = "archive me"
        let archived = try? Data.archive(object)
        XCTAssertNotNil(archived)
    }
    
    func testUnarchive() {
        let original = "test string"
        let archived = try? Data.archive(original)
        XCTAssertNotNil(archived)
        
        let unarchived: String? = archived?.unarchive()
        XCTAssertEqual(original, unarchived)
    }
    
    //-- MARK: handling
    
    func testSubdata() {
        let data = "Hello, World!".data(using: .utf8)!
        let subdata = data.subdata(in: 0...4)
        XCTAssertEqual(subdata.utf8String, "Hello")
    }
    
    func testSplit() {
        let data = "HelloWorld".data(using: .utf8)!
        var chunks: [Data] = []
        
        data.split(size: 5) { chunk in
            chunks.append(chunk)
        }
        
        XCTAssertEqual(chunks.count, 2)
        XCTAssertEqual(chunks[0].utf8String, "Hello")
        XCTAssertEqual(chunks[1].utf8String, "World")
    }
}
