//
//  FloatSpecs.swift
//  AppFrameworkTests
//
//  Created by Jorge Miguel on 19/09/2023.
//  Copyright © 2023 JMIguel. All rights reserved.
//

import XCTest

@testable import AppFrameworkCore

class FloatSpecs: XCTestCase {
    
        func testFloatString() {
            let float = 10.39173
            let floatStr1 = "10.39"
            let floatStr2 = "10.392"
            let floatStr3 = "10"
            XCTAssertEqual(float.string(decimals: 2), floatStr1)
            XCTAssertEqual(float.string(decimals: 3), floatStr2)
            XCTAssertEqual(float.string(decimals: 0), floatStr3)
        }
}
