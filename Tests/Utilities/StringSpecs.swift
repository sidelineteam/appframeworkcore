//
//  StringSpecs.swift
//  AppFrameworkTests
//
//  Created by Jorge Miguel on 27/04/2020.
//  Copyright © 2020 JMIguel. All rights reserved.
//

import XCTest
import SwiftUI

@testable import AppFrameworkCore

class StringSpecs: XCTestCase {
    
    //-- MARK: url

    func testURL() {
        let urlStr = "http://address.com"
        let url = urlStr.url
        XCTAssertEqual(url?.absoluteString, urlStr)
    }
    
    //-- MARK: isEmptyOrWhiteSpaces
    
    func testEmpty() {
        let str1 = ""
        let str2 = "      "
        let str3 = "    \n"
        
        XCTAssertTrue(str1.isEmptyOrWhiteSpaces)
        XCTAssertTrue(str2.isEmptyOrWhiteSpaces)
        XCTAssertTrue(str3.isEmptyOrWhiteSpaces)
    }
    
    //-- MARK: color
    
    func testColor() {
        let goodColor1 = "#000000"
        let goodColor2 = "000000"
        let badColor = "notAColor"
        
        XCTAssertEqual(goodColor1.color, Color.black)
        XCTAssertEqual(goodColor2.color, Color.black)
        XCTAssertEqual(badColor.color, Color.white)
    }
    
    //-- MARK: localized
    
    func testLocalizedWithElements() {
        let str = "x + y = ${z}"
        
        XCTAssertEqual(str.localized([:]), "x + y = ${z}")
        XCTAssertEqual(str.localized(["z": "1"]), "x + y = 1")
        XCTAssertEqual(str.localized(["x": "1", "y": "2"]), "x + y = ${z}")
        XCTAssertEqual(str.localized(["x": "1", "y": "2", "z": "1"]), "x + y = 1")
    }
    
    //-- MARK: JSON Dictionary
    
    func testJSONDictionary() {
        let jsonString = "{\"key\":\"value\"}"
        let dict = jsonString.jsonDictionary
        
        XCTAssertNotNil(dict)
        XCTAssertEqual(dict?["key"] as? String, "value")
        
        let invalidJSON = "not a json"
        XCTAssertNil(invalidJSON.jsonDictionary)
    }
    
    //-- MARK: sha256
    
    func testSHA256() {
        let txt = "test"
        let hash = "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08"
        XCTAssertEqual(txt.sha256, hash)
    }
    
    //-- MARK: encryption
        
    func testEncryption() {
        let originalString = "test string"
        let salt = "salt123"
        
        let encrypted = originalString.encrypt(salt)
        XCTAssertNotNil(encrypted)
        
        let decrypted = encrypted?.decryptString(salt)
        XCTAssertEqual(decrypted, originalString)
    }
    
    //-- MARK: initials
    
    func testInitials() {
        let fullName = "John Doe"
        XCTAssertEqual(fullName.initials, "JD")
        
        let singleName = "John"
        XCTAssertEqual(singleName.initials, "J")
    }
    
    //-- MARK: text format
    
    func testRemovePrefix() {
        let prefix1 = "not_in_the_string"
        let prefix2 = "prefix"
        let string = "prefixed_string_not_in_the_string"
        XCTAssertTrue(string.removePrefix(prefix2) == "ed_string_not_in_the_string")
        XCTAssertTrue(string.removePrefix(prefix1) == string)
    }
    
    //-- MARK: date formatting
    
    func testISODate() {
        let isoString = "2023-01-01T12:00:00Z"
        XCTAssertNotNil(isoString.isoDate)
        
        let invalidISOString = "2023-01-01"
        XCTAssertNil(invalidISOString.isoDate)
    }
    
    func testCustomDateFormat() {
        let dateString = "2023-01-01"
        let format = "yyyy-MM-dd"
        let date = dateString.date(format)
        XCTAssertNotNil(date)
        
        let invalidDateString = "2023-13-01"
        XCTAssertNil(invalidDateString.date(format))
    }
    
    //-- MARK: random string
    
    func testRandomString() {
        let random1 = String.random()
        let random2 = String.random()
        
        XCTAssertEqual(random1.count, 20) // Default length
        XCTAssertEqual(String.random(length: 10).count, 10) // Custom length
        XCTAssertNotEqual(random1, random2) // Should be different
    }
    
    //-- MARK: validations
    
    func testIsEmail() {
        let emailValid = "email@domain.com"
        let emailInvalid1 = "emaildomain.com"
        let emailInvalid2 = "@emaildomain.com"
        let emailInvalid3 = "email@domaincom"
        let emailInvalid4 = "email.@domain.com"
        
        XCTAssertTrue(emailValid.isValidEmail)
        XCTAssertFalse(emailInvalid1.isValidEmail)
        XCTAssertFalse(emailInvalid2.isValidEmail)
        XCTAssertFalse(emailInvalid3.isValidEmail)
        XCTAssertFalse(emailInvalid4.isValidEmail)
    }
    
    func testIsUrl() {
        let urlValid1 = "https://www.google.com/something?var=1"
        let urlValid2 = "https://google.com"
        let urlInvalid1 = "htps://www.google.com"
        let urlInvalid2 = "https:/google.com"
        let urlInvalid3 = "https://www.googlecom"
        
        XCTAssertTrue(urlValid1.isValidUrl)
        XCTAssertTrue(urlValid2.isValidUrl)
        XCTAssertFalse(urlInvalid1.isValidUrl)
        XCTAssertFalse(urlInvalid2.isValidUrl)
        XCTAssertFalse(urlInvalid3.isValidUrl)
    }
    
    //-- MARK: regex
    
    func testRegex1() {
        let regex = "fix([0-9])([0-9])"
        let string = "prefix12 aaa3 sufix45"
        let result = [["fix12", "1", "2"], ["fix45", "4", "5"]]
        
        let vals = string.match(regex)
        
        XCTAssertEqual(vals, result)
    }
    
    func testRegex2() {
        let regex = "(?:prefix)?([0-9]+)"
        let string = "prefix12"
        let result = [["prefix12", "12"]]
        
        let vals = string.match(regex)
        
        XCTAssertEqual(vals, result)
    }
    
    func testRegex3() {
        let regex = "(?:prefix)?([0-9]+)"
        let string = "12"
        let result = [["12", "12"]]
        
        let vals = string.match(regex)
        
        XCTAssertEqual(vals, result)
    }
    
    //-- MARK: base64 encoding
    
    func testBase64Encoding() {
        let original = "Hello, World!"
        let expected = "SGVsbG8sIFdvcmxkIQ=="
        
        XCTAssertEqual(original.encodeToBase64, expected)
    }
    
    //-- MARK: JWT Decoder
    
    func testJwtTokenDictionary() {
        let jwt = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
        let info: [String : Any] = ["sub": "1234567890", "name": "John Doe", "iat": 1516239022]
        
        let decoded = try? jwt.jwtTokenDictionary
        
        XCTAssertEqual(decoded?["sub"] as! String , info["sub"] as! String)
        XCTAssertEqual(decoded?["name"] as! String, info["name"] as! String)
        XCTAssertEqual(decoded?["iat"]as! Int, info["iat"]as! Int)
    }
}
