//
//  PHAsset.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 14/10/2018.
//  Copyright (c) 2018 Jorge Miguel. All rights reserved.
//

#if os(iOS)

import UIKit
import Photos.PHAsset

//--

public typealias AssetFileURLHandler = (_ responseURL: URL?) -> Void

public typealias AssetImageHandler = (_ image: UIImage?) -> Void
public typealias AssetImageDataHandler = (_ data: Data?, _ orientation: CGImagePropertyOrientation?) -> Void

//--

extension PHAsset {

    public var fileName: String? {
        var fname:String?

        let resources = PHAssetResource.assetResources(for: self)
        if let resource = resources.first {
            fname = resource.originalFilename
        }

        if fname == nil {
            // this is an undocumented workaround that works as of iOS 9.1
            fname = self.value(forKey: "filename") as? String
        }

        return fname
    }

    public var fileSize: Int64? {
        let resources = PHAssetResource.assetResources(for: self)
        if let resource = resources.first {
            let unsignedInt64 = resource.value(forKey: "fileSize") as? CLong
            return Int64(bitPattern: UInt64(unsignedInt64!))
        }

        return nil
    }
    
    public func fileUrl(_ handler : @escaping AssetFileURLHandler) {

        if self.mediaType == .image {
            let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
            options.canHandleAdjustmentData = {(adjustmeta: PHAdjustmentData) -> Bool in
                return true
            }

            self.requestContentEditingInput(with: options, completionHandler: { (contentEditingInput, info) in
                handler(contentEditingInput!.fullSizeImageURL)
            })

        } else if self.mediaType == .video {
            let options: PHVideoRequestOptions = PHVideoRequestOptions()
            options.version = .original
            PHImageManager.default().requestAVAsset(forVideo: self, options: options, resultHandler: { (asset, audioMix, info) in
                if let urlAsset = asset as? AVURLAsset {
                    let localVideoUrl = urlAsset.url
                    handler(localVideoUrl)

                } else {
                    handler(nil)
                }
            })
        }
    }

    public func thumbnail(width: Int, height: Int, synchronized: Bool = false, handler : @escaping AssetImageHandler) {
        let options = PHImageRequestOptions()
        options.deliveryMode = .opportunistic
        options.isNetworkAccessAllowed = false
        options.isSynchronous = synchronized
        options.normalizedCropRect = CGRect.zero
        options.progressHandler = nil
        options.resizeMode = .fast
        options.version = .current

        PHImageManager.default().requestImage(for: self, targetSize: CGSize(width: width, height: height),
                                              contentMode: .aspectFit, options: options, resultHandler: { (result, info)->Void in
            handler(result)
        })
    }

    public func thumbnail(synchronized: Bool = false, handler : @escaping AssetImageHandler) {
        guard #available(iOS 13, *) else {
            self.thumbnail(width: 500, height: 500, synchronized: synchronized, handler: handler)
            return
        }
        
        let options = PHImageRequestOptions()
        options.deliveryMode = .opportunistic
        options.isNetworkAccessAllowed = false
        options.isSynchronous = synchronized
        options.normalizedCropRect = CGRect.zero
        options.progressHandler = nil
        options.resizeMode = .fast
        options.version = .current

        PHImageManager.default().requestImageDataAndOrientation(for: self, options: nil) { (data, _, _, _) in
            guard let data = data else {
                handler(nil)
                return
            }

            handler(UIImage(data: data))
        }
    }

    public func fullResolution(synchronized: Bool = false, handler : @escaping AssetImageHandler) {
        self.data(synchronized: synchronized) { (data, orientation) in
            guard let data = data else {
                handler(nil)
                return
            }
            
            handler(UIImage(data: data)?.normalizedOrientation)
        }
    }

    public func data(synchronized: Bool = false, handler : @escaping AssetImageDataHandler) {
        let options = PHImageRequestOptions()
        options.deliveryMode = .highQualityFormat
        options.isNetworkAccessAllowed = false
        options.isSynchronous = synchronized
        options.normalizedCropRect = CGRect.zero
        options.progressHandler = nil
        options.resizeMode = .none
        options.version = .current

        if #available(iOS 13, *) {
            PHImageManager.default().requestImageDataAndOrientation(for: self, options: options) { (data, _, orientation, _) in
                handler(data, orientation)
            }
        } else {
            PHImageManager.default().requestImageData(for: self, options: options) { (data, _, _, _) in
                handler(data, .up)
            }
        }
    }
}

#endif
