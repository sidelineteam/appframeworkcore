//
//  Int64Specs.swift
//  AppFrameworkTests
//
//  Created by Jorge Miguel on 27/04/2020.
//  Copyright © 2020 JMIguel. All rights reserved.
//

import XCTest

@testable import AppFrameworkCore

class Int64Specs: XCTestCase {
    
    func testIntValue() {
        let val1: Int = 123456
        let val2: Int64 = 123456
        
        XCTAssertEqual(val1, val2.intValue)
    }
    
    func testStringValue() {
        let val1: Int64 = 123456
        let val2: String = "123456"
        
        XCTAssertEqual(val1.stringValue, val2)
    }

    func testHumanReadableFormat() {
        var value: Int64 = 10*1000 //-- 10KB
        XCTAssertEqual(value.humanReadableFormat, "10.0 KB")
        
        value = value*1000 //-- 10MB
        XCTAssertEqual(value.humanReadableFormat, "10.0 MB")
        
        value = value*1000 //-- 10GB
        XCTAssertEqual(value.humanReadableFormat, "10.0 GB")
        
        value = value*1000 //-- 10TB
        XCTAssertEqual(value.humanReadableFormat, "10.0 TB")
        
        value = value*1000 //-- 10PB
        XCTAssertEqual(value.humanReadableFormat, "10.0 PB")
    }
    
    func testUInt64UnsignedIntValue() {
        let val1: UInt = 123456
        let val2: UInt64 = 123456
        
        XCTAssertEqual(val1, val2.unsignedIntValue)
    }
    
    func testUInt64StringValue() {
        let val1: UInt64 = 123456
        let val2: String = "123456"
        
        XCTAssertEqual(val1.stringValue, val2)
    }
    
    func testUInt64HumanReadableFormat() {
        // Test bytes
        var value: UInt64 = 900
        XCTAssertEqual(value.humanReadableFormat, "900 B")
        
        // Test KB
        value = 10*1000 //-- 10KB
        XCTAssertEqual(value.humanReadableFormat, "10.0 KB")
        
        // Test MB
        value = value*1000 //-- 10MB
        XCTAssertEqual(value.humanReadableFormat, "10.0 MB")
        
        // Test GB
        value = value*1000 //-- 10GB
        XCTAssertEqual(value.humanReadableFormat, "10.0 GB")
        
        // Test TB
        value = value*1000 //-- 10TB
        XCTAssertEqual(value.humanReadableFormat, "10.0 TB")
        
        // Test PB
        value = value*1000 //-- 10PB
        XCTAssertEqual(value.humanReadableFormat, "10.0 PB")
        
        // Test EB
        value = value*1000 //-- 10EB
        XCTAssertEqual(value.humanReadableFormat, "10.0 EB")
    }
}
