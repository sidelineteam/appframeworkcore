//
//  MirrorSpecs.swift
//  AppFrameworkTests
//
//  Created by Jorge Miguel on 05/06/2021.
//  Copyright © 2021 JMIguel. All rights reserved.
//

import XCTest

@testable import AppFrameworkCore

class MirrorSpecs: XCTestCase {
    
    var var1: String = "aString"
    var var2: String = "anotherString"
    var var3: Int = 1
    var var4: Int = 10
    
    func testReflectProperties() {
        
        var it = 0
        var tot = 0
        Mirror.reflectProperties(of: self) { (name, value: Int) in
            it += 1
            tot += value
            
            Log.debug(self.methodName(), "\(name) = \(value)")
            return false
        }
        
        XCTAssertEqual(it, 2)
        XCTAssertEqual(tot, 11)
    }
}
