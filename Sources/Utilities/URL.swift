//
//  URL.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 17/05/2020.
//  Copyright (c) 2020 Jorge Miguel. All rights reserved.
//

import Foundation

extension URL {
    
    public func relativePath(from base: URL) -> String? {
        
        // Ensure that both URLs represent files:
        guard self.isFileURL && base.isFileURL else {
            return nil
        }

        // Need to use workBase in lower part
        var workBase = base
        if workBase.pathExtension != "" {
            workBase = workBase.deletingLastPathComponent()
        }

        // Remove/replace "." and "..", make paths absolute:
        let destComponents = self.standardized.resolvingSymlinksInPath().pathComponents
        let baseComponents = workBase.standardized.resolvingSymlinksInPath().pathComponents

        // Find number of common path components:
        var i = 0
        while i < destComponents.count &&
              i < baseComponents.count &&
              destComponents[i] == baseComponents[i] {
                i += 1
        }

        // Build relative path:
        var relComponents = Array(repeating: "..", count: baseComponents.count - i)
        relComponents.append(contentsOf: destComponents[i...])
        
        return relComponents.joined(separator: "/")
    }
    
    public func moveFile(to url: URL? = nil, as name: String) -> URL {
        let destinationURL: URL = {
            if let url = url {
                return url.appendingPathComponent(name)
                
            } else {
                return self.deletingLastPathComponent().appendingPathComponent(name)
            }
        }()

        do {
            if FileManager.default.fileExists(atPath: destinationURL.path) {
                try FileManager.default.removeItem(at: destinationURL)
            }
            
            try FileManager.default.moveItem(at: self, to: destinationURL)
            
        } catch {
            return self
        }
        
        return destinationURL
    }
    
    public func queryItem(_ name: String) -> String? {
        guard let url = URLComponents(url: self, resolvingAgainstBaseURL: false) else {
            return nil
        }
        
        return url.queryItems?.first(where: { $0.name == name })?.value
    }
    
    public func removingQueryItem(_ name: String) -> URL? {
        let string = self.absoluteString
        guard let pos = string.range(of: "\(name)=")?.lowerBound else {
            return self
        }
        
        //-- has query items after the one being removed?
        guard let remain = string.suffix(from: pos).range(of: "&")?.upperBound else {
            return String(string.prefix(upTo: pos).dropLast()).url
        }
        
        return "\(string.prefix(upTo: pos))\(string.suffix(from: remain))".url
    }
}
