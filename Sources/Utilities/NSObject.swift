//
//  NSObject.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 31/10/2018.
//  Copyright (c) 2018 Jorge Miguel. All rights reserved.
//

import Foundation

extension NSObject {

    public func synchronized<T>(_ lockObj: AnyObject, closure: () throws -> T) rethrows -> T {
        objc_sync_enter(lockObj)
        defer {
            objc_sync_exit(lockObj)
        }

        return try closure()
    }
}
