//
//  Color.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 29/04/2020.
//  Copyright (c) 2020 Jorge Miguel. All rights reserved.
//

import SwiftUI
import Foundation

extension Color {
    
    public init(hex: String) {
        let color = hex.replacingOccurrences(of: "#", with: "")
        guard color.count == 6 else {
            self = .white
            return
        }

        var rgbValue: UInt64 = 0
        Scanner(string: color).scanHexInt64(&rgbValue)

        self = Color(red:   Double((rgbValue & 0xFF0000) >> 16) / 255.0,
                     green: Double((rgbValue & 0x00FF00) >> 8) / 255.0,
                     blue:  Double(rgbValue & 0x0000FF) / 255.0)
    }
    
    public var uiColor: UIColor {
        let hex = self.description.replacingOccurrences(of: "#", with: "")
        guard hex.count == 8 else {
            return .white
        }

        var rgbValue: UInt64 = 0
        Scanner(string: hex).scanHexInt64(&rgbValue)
        
        return UIColor(red:   CGFloat((rgbValue & 0xFF000000) >> 24) / 255.0,
                       green: CGFloat((rgbValue & 0x00FF0000) >> 16) / 255.0,
                       blue:  CGFloat((rgbValue & 0x0000FF00) >> 8) / 255.0,
                       alpha: CGFloat(rgbValue & 0x000000FF) / 255.0)
    }
}

@available(iOS 14.0, *)
extension Color: Codable {
    
    enum CodingKeys: String, CodingKey {
        case red, green, blue
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let r = try container.decode(Double.self, forKey: .red)
        let g = try container.decode(Double.self, forKey: .green)
        let b = try container.decode(Double.self, forKey: .blue)
        
        self.init(red: r, green: g, blue: b)
    }

    public func encode(to encoder: Encoder) throws {
        guard let colorComponents = self.colorComponents else {
            return
        }
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(colorComponents.red, forKey: .red)
        try container.encode(colorComponents.green, forKey: .green)
        try container.encode(colorComponents.blue, forKey: .blue)
    }
    
    #if os(macOS)
    typealias SystemColor = NSColor
    #else
    typealias SystemColor = UIColor
    #endif
    
    public var colorComponents: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)? {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        #if os(macOS)
        //-- on RGB color will raise an exception
        SystemColor(self).getRed(&r, green: &g, blue: &b, alpha: &a)
        
        #else
        guard SystemColor(self).getRed(&r, green: &g, blue: &b, alpha: &a) else {
            //-- The color should be convertible into RGB format
            //-- Colors using hue, saturation and brightness won't work
            return nil
        }
        #endif
        
        return (r, g, b, a)
    }
}
