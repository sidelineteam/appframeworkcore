//
//  DoubleSpecs.swift
//  AppFrameworkTests
//
//  Created by Jorge Miguel on 19/09/2023.
//  Copyright © 2023 JMIguel. All rights reserved.
//

import XCTest

@testable import AppFrameworkCore

class DoubleSpecs: XCTestCase {
    
        func testDoubleString() {
            let double = 10.39173
            let doubleStr1 = "10.39"
            let doubleStr2 = "10.392"
            let doubleStr3 = "10"
            XCTAssertEqual(double.string(decimals: 2), doubleStr1)
            XCTAssertEqual(double.string(decimals: 3), doubleStr2)
            XCTAssertEqual(double.string(decimals: 0), doubleStr3)
        }
}
