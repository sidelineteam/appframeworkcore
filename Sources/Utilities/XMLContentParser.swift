//
//  XMLContentParser.swift
//  AppFrameworkV2
//
//  Created by Jorge Miguel on 24/03/2021.
//  Copyright (c) 2021 Jorge Miguel. All rights reserved.
//

import Foundation

public typealias XMLContentParserHandler = (_ content: [String:String]) -> Void

public class XMLContentParser : NSObject, XMLParserDelegate {
    
    private let handler: XMLContentParserHandler
    
    private let parser: XMLParser
    
    //-- elements to look for on the xml
    private var elements: [String]
    
    //-- current element being parsed
    private var element: String?
    
    //-- elements found on the xml
    private var parsedElements = [String:String]()
    
    //--
    
    public init(with parser: XMLParser, find elements: [String], handler: @escaping XMLContentParserHandler) {
        self.elements = elements.compactMap { $0.lowercased() }
        self.handler = handler
        self.parser = parser
        
        super.init()
        
        parser.delegate = self
        parser.parse()
    }
    
    //-- MARK: XMLParser delegate
    
    public func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        self.element = elementName.lowercased()
    }
    
    public func parser(_ parser: XMLParser, foundCharacters string: String) {
        guard let element = self.element else {
            return
        }
        
        if self.elements.contains(element) {
            let content = string.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.parsedElements[element] = content
        }
        
        self.element = nil
        
        if self.parsedElements.count == 3 {
            parser.abortParsing()
            
            self.handler(self.parsedElements)
        }
    }
    
    public func parserDidEndDocument(_ parser: XMLParser) {
        self.handler(self.parsedElements)
    }
}
